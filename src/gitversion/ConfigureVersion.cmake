# CMake script that configures the version header information

execute_process(
    COMMAND ${GIT_EXECUTABLE} describe --always --long
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_DESCRIBE
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

string(REGEX MATCH "^([0-9]+\\.[0-9]+)?(.*)$" VERSION_TAG ${GIT_DESCRIBE})
if (DEFINED CMAKE_MATCH_1)
    string(REGEX MATCH "^([0-9]+)\\.([0-9]+)-([0-9]+)-(.*)$" VERSION_NUM ${GIT_DESCRIBE})
    set(APP_MAJOR ${CMAKE_MATCH_1})
    set(APP_MINOR ${CMAKE_MATCH_2})
    set(APP_COMMIT ${CMAKE_MATCH_3})
    set(APP_HASH ${CMAKE_MATCH_4})
    set(APP_FULL "${APP_MAJOR}.${APP_MINOR}")
    if (${APP_COMMIT} GREATER 0)
        set(APP_FULL "${APP_FULL}+${APP_COMMIT}-${APP_HASH}")
    endif()
else ()
    set(APP_MAJOR 0)
    set(APP_MINOR 0)
    set(APP_COMMIT 0)
    set(APP_HASH ${GIT_DESCRIBE})
    set(APP_FULL ${APP_HASH})
endif ()

message( STATUS "Configure ${GITVERSION_OUT}.new" )
configure_file(
    ${GITVERSION_IN}
    ${GITVERSION_OUT}.new
)

# get hashes of new and old version header file
file( MD5 ${GITVERSION_OUT}.new MD5_NEW)
# message("Looking for ${GITVERSION_OUT}.new -> ${MD5_NEW}")

if ( EXISTS ${GITVERSION_OUT} )
    file( MD5 ${GITVERSION_OUT} MD5_CUR)
    # message("Found ${GITVERSION_OUT} -> ${MD5_CUR}")
else ()
    # message( STATUS "No version.hpp found" )
    set( MD5_CUR 0 )
endif ()

# substitute header if needed
if ( NOT "${MD5_NEW}" STREQUAL "${MD5_CUR}" )
    # message("Updating version.hpp")
    file( RENAME ${GITVERSION_OUT}.new ${GITVERSION_OUT} )
else ()
    # message("Keeping old version.hpp")
    file( REMOVE ${GITVERSION_OUT}.new )
endif()

