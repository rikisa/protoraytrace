#pragma once
// not nessecarily needed, but i like to be explicit
extern "C" {
// this order to let GLFW know that we manage extension ourselfes
// but to be explicit... Allows to change order as we see fit
#define GLFW_INCLUDE_NONE
// #include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glad/glad.h>
}

#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

typedef unsigned int uint_t;

/*
 * Shared memory object between observer and whatever writes to it
 * Only threadsave if there are only observers an is mutated by one
 */
class CanvasData {

public:
  CanvasData( uint_t width, uint_t height, uint_t channels );
  void const *raw();
  void update();
  std::vector<uint8_t> data;

private:
  uint_t const width;
  uint_t const height;
  uint_t const channels;
  uint_t tex_id;
};

/*
 * CanvasObject storing all vertice data and opengl buffer data
 * Combines vertex array, buffer array element buffer and everything
 * else needed to render the canvas
 */
class CanvasObject {

public:
  CanvasObject();

  void scale( float factor ) {
    for ( int i = 0; i < 4; ++i ) {
      for ( int j = 0; j < 4; ++j ) {
        this->object_data[ i * 8 + j ] *= factor;
      }
    }
  }
  void update_verts( void );

private:
  // triangle vertices
  std::vector<float> object_data = {
      // positions          // colors           // texture coords
      0.5f,  0.5f,  0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top right
      0.5f,  -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // bottom right
      -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom left
      -0.5f, 0.5f,  0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f  // top left
  };
  std::vector<unsigned int> indices = {
      // note that we start from 0!
      0, 1, 3, // first triangle
      1, 2, 3  // second triangle
  };

  uint_t vertarr_id;
  uint_t arrbuff_id;
  uint_t elembuff_id;
};

/*
 * Very thin and basic abstraction of OpenGL ShaderPrograms
 * Hard coded shader code sufficient for the RenderObervers
 */
class CanvasShaderProgram {

public:
  CanvasShaderProgram();
  void use();
  int id();

private:
  std::string vertex_shader_src = "#version 330 core\n"
                                  "layout (location = 0) in vec3 aPos;\n"
                                  "layout (location = 1) in vec3 aColor;\n"
                                  "layout (location = 2) in vec2 aTexCoord;\n"
                                  "uniform vec2 param;\n"
                                  "vec3 offset;\n"
                                  "out vec3 ourColor;\n"
                                  "out vec2 TexCoord;\n"
                                  "void main()\n"
                                  "{\n"
                                  "   offset = vec3(1 + sin(param[1] * 1e-2), 1, 1);\n"
                                  "   gl_Position = vec4(aPos * offset , param[0]);\n"
                                  "   ourColor = aColor;\n"
                                  "   TexCoord = aTexCoord;\n"
                                  "}\0";

  std::string fragment_shader_src =
      "#version 330 core\n"
      "out vec4 FragColor;\n"
      "in vec3 ourColor;\n"
      "in vec2 TexCoord;\n"
      "uniform sampler2D ourTexture;\n"
      "void main()\n"
      "{\n"
      "    // FragColor = vec4(ourColor, 1.0f);\n"
      "    // FragColor = texture(ourTexture, TexCoord) * pow(TexCoord.x - TexCoord.y, 2);\n"
      "    FragColor = texture(ourTexture, TexCoord);\n"
      "    // FragColor = vec4(ourColor.x, ourColor.y, ourColor.z, 1.0f);\n"
      "} \n";

  uint_t vertsh_id;
  uint_t fragsh_id;
  uint_t prog_id;

  void compile_shader( uint_t &shader_id, std::string &source );
  void check_compile( uint_t const &shader_id );
};

/*
 * Very thin and basic abstraction of OpenGL context and window
 * Observes and displays a raw data buffer
 */
class RenderObserver {
public:
  RenderObserver( uint_t width, uint_t height, std::string const title );
  void observe( CanvasData const &canvas );
  GLFWwindow *get();
  float zoom;

private:
  std::shared_ptr<GLFWwindow> window;
  std::shared_ptr<CanvasData const> observee;

  static void print_error( int error, char const *description ) {
    std::cout << "Error: " << description;
  };

  static void key_callback( GLFWwindow *window, int key, int scancode, int action, int mods ) {
    float *zoom_ptr = (float *)glfwGetWindowUserPointer( window );
    if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS ) {
      glfwSetWindowShouldClose( window, GLFW_TRUE );
    } else if ( key == GLFW_KEY_KP_ADD && action == GLFW_PRESS ) {
      ( *zoom_ptr ) += 0.1;
      std::cout << "Zoom now " << *zoom_ptr << "\n";
    } else if ( key == GLFW_KEY_KP_SUBTRACT && action == GLFW_PRESS ) {
      float *zoom_ptr = (float *)glfwGetWindowUserPointer( window );
      ( *zoom_ptr ) -= 0.1;
      std::cout << "Zoom now " << *zoom_ptr << "\n";
    } else {
      std::cout << key << ", " << scancode << ", " << action << ", " << mods << "\n";
    }
  } // key_callback

  static void fb_size_callback( GLFWwindow *window, int width, int height ) {
    glClear( GL_COLOR_BUFFER_BIT );
    glViewport( 0, 0, width, height );
  };

}; // RenderObserver
