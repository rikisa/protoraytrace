#include "viewer.hpp"

CanvasData::CanvasData( uint_t width, uint_t height, uint_t channels )
    : width( width ), height( height ), channels( channels ), data( width * height * channels ) {

  glGenTextures( 1, &this->tex_id );
  glActiveTexture( GL_TEXTURE0 ); // activating texture unit to bind to
  glBindTexture( GL_TEXTURE_2D, this->tex_id );

  // operate on binded 2d texture, but not needed for now
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
};

void const *CanvasData::raw() { return static_cast<void *>( this->data.data() ); }

/* Update and set new texture to use from this->data
 */
void CanvasData::update() {
  glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGB, this->width, this->height, 0, GL_RGB, GL_UNSIGNED_BYTE,
      this->raw() );
}; // CanvasData::update

RenderObserver::RenderObserver( uint_t width, uint_t height, std::string const title ) {
  glfwSetErrorCallback( this->print_error );

  if ( !glfwInit() ) {
    // Initialization failed
    std::cout << "Could not initialize OpenGL\n";
  } else {
    std::cout << "OpenGL initialized\n";
  }

  // Opaque GLFWwindow can be wrapped if destructor call is passed as well
  // Handle for a window with some opengl context (state)
  this->window = std::shared_ptr<GLFWwindow>(
      glfwCreateWindow( 640, 480, "My Title", NULL, NULL ), glfwDestroyWindow );

  if ( !this->window ) {
    std::cout << "Window and context creation failed\n";
    glfwTerminate(); // really needed?
  } else {
    std::cout << "Created window\n";
  }

  glfwMakeContextCurrent( this->window.get() );

  // let GLAD work on everything GLFW has build up so far
  auto procAddr = (GLADloadproc)glfwGetProcAddress;
  if ( !gladLoadGLLoader( procAddr ) ) {
    std::cout << "Could not load OpenGL entry points\n";
  } else {
    std::cout << "OpenGL entry points loaded\n"
              << "\t OpenGL Version: " << glGetString( GL_VERSION ) << "\n"
              << "\t GLSL Version: " << glGetString( GL_SHADING_LANGUAGE_VERSION ) << "\n"
              << "\n";
  }

  // get frambuffer size and set viewport
  int dyn_width, dyn_height;
  glfwGetFramebufferSize( this->window.get(), &dyn_width, &dyn_height );
  glViewport( 0, 0, dyn_width, dyn_height );
  if ( dyn_width != width or dyn_height != height ) {
    std::cout << "Requested window size (" << width << ", " << height << ")"
              << "but buffer is (" << dyn_width << ", " << dyn_height << ")\n";
  }

  // set callbacks
  glfwSetFramebufferSizeCallback( this->window.get(), this->fb_size_callback );
  glfwSetKeyCallback( this->window.get(), this->key_callback );

  // vsync
  glfwSwapInterval( 1 );

  // set color for glClear (background color) clear buffer to background
  glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT ); // also: GL_DEPTH_BUFFER_BIT, GL_STENCIL_BUFFER_BIT

  // set user pointers
  // glfwSetWindowUserPointer(this->window.get(), zoom_ptr );
  this->zoom = 1.0;
  glfwSetWindowUserPointer( this->window.get(), (void *)&this->zoom );
}

GLFWwindow *RenderObserver::get() { return this->window.get(); }

void CanvasObject::update_verts( void ) {
  glBufferData(
      GL_ARRAY_BUFFER, sizeof( this->object_data[ 0 ] ) * this->object_data.size(),
      this->object_data.data(),
      GL_STATIC_DRAW // affects mem allocaition on GPU
                     //                // static: changed once, read often; dynamic + changed often
                     // GL_DYNAMIC_DRAW // affects mem allocaition on GPU
  );
}

CanvasObject::CanvasObject() {

  // bind all stuff follwoing to Vertex array object
  glGenVertexArrays( 1, &this->vertarr_id );
  glBindVertexArray( this->vertarr_id );

  // create buffer object and bind to specific (unique) buffer type
  // here array buffere aka vertex buffer
  // binding -> reference buffer we operate on
  glGenBuffers( 1, &this->arrbuff_id );
  glBindBuffer( GL_ARRAY_BUFFER, this->arrbuff_id );
  glBufferData(
      GL_ARRAY_BUFFER, sizeof( this->object_data[ 0 ] ) * this->object_data.size(),
      this->object_data.data(),
      // GL_STATIC_DRAW // affects mem allocaition on GPU
      //                // static: changed once, read often; dynamic + changed often
      GL_DYNAMIC_DRAW // affects mem allocaition on GPU
  );

  // Element buffser for aggregating shared vertices into elements
  // Needs a bound array buffer
  glGenBuffers( 1, &this->elembuff_id );
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->elembuff_id );
  glBufferData(
      GL_ELEMENT_ARRAY_BUFFER, sizeof( this->indices[ 0 ] ) * this->indices.size(),
      this->indices.data(), GL_STATIC_DRAW );

  /* BUFFER LAYOUT
   * tell opengl how to interpret data in vertex buffer
   * will always refer to current vertex buffer
   * will be bound to current VAO
   */

  // vertices pos
  glVertexAttribPointer(
      0,        // index for this attribute
      3,        // elements per vertice
      GL_FLOAT, // type
      GL_FALSE, // normalize/clamp to -1,1 / 0,1
      /* number of bytes per vertex attribute
       * can be deduced from type and
       * size param, if tightly packed
       */
      8 * sizeof( this->object_data[ 0 ] ),
      (void *)0 // void pointer to first element
  );

  // vertices color
  glVertexAttribPointer(
      1,        // index for this attribute
      3,        // elements per vertice
      GL_FLOAT, // type
      GL_FALSE, // normalize/clamp to -1,1 / 0,1
      /* number of bytes per vertex
       * can be deduced from type and
       * size param, if tightly packed
       */
      8 * sizeof( object_data[ 0 ] ),
      (void *)( 3 * sizeof( object_data[ 0 ] ) ) // void pointer to first element
  );

  // vertices texture mapping
  glVertexAttribPointer(
      2,        // index for this attribute
      2,        // elements per vertice
      GL_FLOAT, // type
      GL_FALSE, // normalize/clamp to -1,1 / 0,1
      /* number of bytes per vertex
       * can be deduced from type and
       * size param, if tightly packed
       */
      8 * sizeof( object_data[ 0 ] ),
      (void *)( 6 * sizeof( object_data[ 0 ] ) ) // void pointer to first element
  );

  glEnableVertexAttribArray( 0 );
  glEnableVertexAttribArray( 1 );
  glEnableVertexAttribArray( 2 );

}; // CanvasObject::CanvasObject

CanvasShaderProgram::CanvasShaderProgram() {

  this->vertsh_id = glCreateShader( GL_VERTEX_SHADER );
  this->fragsh_id = glCreateShader( GL_FRAGMENT_SHADER );

  this->compile_shader( this->vertsh_id, this->vertex_shader_src );
  this->compile_shader( this->fragsh_id, this->fragment_shader_src );

  this->check_compile( this->vertsh_id );
  this->check_compile( this->fragsh_id );

  this->prog_id = glCreateProgram();
  glAttachShader( this->prog_id, this->vertsh_id );
  glAttachShader( this->prog_id, this->fragsh_id );
  glLinkProgram( this->prog_id );
  // use it...!
  // clean up the (static) linked shaders. they live in the programm now
  glDeleteShader( this->vertsh_id );
  glDeleteShader( this->fragsh_id );
  // glGetProgrammiv, get log and success... omitted

  this->vertsh_id = -1;
  this->fragsh_id = -1;

}; // CanvasShaderProgram::CanvasShaderProgram

void CanvasShaderProgram::compile_shader( uint_t &shader_id, std::string &source ) {
  // how...?
  char const *source_ptr = source.c_str();
  char const **line_ptr = &source_ptr;
  // expects a list of strings aka char**, stringlenght := NULL
  glShaderSource( shader_id, 1, line_ptr, NULL );
  glCompileShader( shader_id );

}; // CanvasShaderProgramm::compile_shader

void CanvasShaderProgram::check_compile( uint_t const &shader_id ) {
  // shader compiling error handling
  int success;
  char infoLog[ 512 ];
  glGetShaderiv( shader_id, GL_COMPILE_STATUS, &success );
  if ( !success ) {
    glGetShaderInfoLog( shader_id, 512, NULL, infoLog );
    std::cout << "SHADER COMPILE ERROR: " << infoLog << "\n";
  } else {
    std::cout << "Shader " << shader_id << " compiled successfully\n";
  }
}

void CanvasShaderProgram::use() { glUseProgram( this->prog_id ); };

int CanvasShaderProgram::id() { return this->prog_id; };
