#include "viewer.hpp"

#include <algorithm>
#include <functional>
#include <random>
#include <thread>

void noisy( CanvasData &content, bool &run ) {
  std::random_device rd;
  std::mt19937 mt( rd() );
  std::uniform_int_distribution<uint8_t> dist( 0, 255 );

  int row_bytes = 640 * 3;
  std::vector<uint8_t> border = { 0, 255, 0 };
  while ( run ) {
    for ( int i = 0; i < 640 * 480 * 3; ++i ) {
      if ( i % row_bytes <= 2 ) {
        content.data[ i ] = border[ i % row_bytes ];
      } else {
        content.data[ i ] = dist( mt );
      }
    }
  }
  std::cout << "Stopping thread\n";
} // noisy

int main( void ) {

  auto obs = RenderObserver( 640, 480, "Observer" );
  auto canvas = CanvasObject();
  auto prog = CanvasShaderProgram();
  auto content = CanvasData( 640, 480, 3 );

  bool run = true;

  int param_loc = glGetUniformLocation( prog.id(), "param" );

  float count = 0;

  // must be wrapped, as thread args are moved or copied
  // goes into renderer
  std::thread crunching( noisy, std::ref( content ), std::ref( run ) );
  prog.use();
  while ( !glfwWindowShouldClose( obs.get() ) ) {

    glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT );

    count += 1;
    // wooden hammer recalc vertices
    // canvas.scale( obs.zoom );
    // canvas.update_verts();
    glUniform2f( param_loc, obs.zoom, count );
    // obs.zoom = 1.0;

    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

    glfwSwapBuffers( obs.get() );
    glfwPollEvents();

    content.update();
  }

  std::cout << "čauky!\n";
  run = false;
  crunching.join();

  // glfwTerminate is cranky, if there is context left...
  // We do not want cranky error messages at the end.
  glfwSetErrorCallback( NULL );
  glfwTerminate();
  return 0;
}
