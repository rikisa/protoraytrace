#include "core/image.hpp"
#include "core/linalg.hpp"

class TermViewerFX {

public:
  TermViewerFX();
  ~TermViewerFX();

  void draw( core::ImageView<core::ColorVec> const & );
  void _update( core::ImageView<core::ColorVec> const &, size_t, size_t );
};
