#include "core/image.hpp"
#include "core/linalg.hpp"

#include <chrono>
#include <clocale>
#include <notcurses/notcurses.h>
#include <thread>

union rgbpack {
  std::uint32_t pack;
  std::array<std::uint8_t, 3> rgb;
};

void draw(
    notcurses *nc, ncplane *plane, core::ImageView<core::ColorVec> const &img, unsigned trows,
    unsigned tcols ) {

  std::vector<unsigned char> packed{};
  for ( auto i = 0; i < img.size; ++i ) {
    auto rowcol = img.unravel_index( i );
    auto px = img.get_pixel( rowcol.first, rowcol.second );
    for ( auto val : px ) {
      packed.push_back( static_cast<unsigned char>( val * 255.0 ) );
    };
    packed.push_back( static_cast<unsigned char>( 255.0 ) );
  }

  ncvisual_options vopts{
      .n = plane,
      .scaling = NCSCALE_SCALE,
      .y = 1,
      .x = 0,
      .begy = 0,
      .begx = 0,
      .leny = static_cast<unsigned>( img.rows ),
      .lenx = static_cast<unsigned>( img.cols ),
      .blitter = NCBLIT_3x2 };
  // auto num_blitted = ncblit_rgba(packed.data(), img.cols * 4, &vopts);

  auto *ncv = ncvisual_from_rgba( packed.data(), img.rows, img.cols * 4, img.cols );
  ncvisual_blit( nc, ncv, &vopts );
}

auto main() -> int {
  (void)setlocale( LC_ALL, "" );

  core::Image<> img( 0, 0 );
  // img.load( "/home/rikisa/Downloads/test.png" );
  img.load( "/home/rikisa/Downloads/ryg_state.png" );
  // img.load( "/home/rikisa/Downloads/rose.png" );

  notcurses_options opts{ .flags = NCOPTION_SUPPRESS_BANNERS };

  auto *nc = notcurses_init( &opts, stdout );

  unsigned dimx = -1;
  unsigned dimy = -1;
  auto *stdplane = notcurses_stddim_yx( nc, &dimy, &dimx );
  auto *useplane = ncplane_dup( stdplane, nullptr );

  draw( nc, useplane, img, dimy, dimx );

  auto ren = notcurses_render( nc );

  ncinput ni; // necessary for mouse
  char32_t key = ' ';
  while ( key != 'q' ) {
    key = notcurses_get_nblock( nc, &ni );
    std::this_thread::sleep_for( std::chrono::milliseconds( 30 ) );
  }

  return notcurses_stop( nc );
}
