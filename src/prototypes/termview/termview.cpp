#include "core/linalg.hpp"
#include "termviewer.h"

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <curses.h>
#include <iostream>
#include <thread>

TermViewer::TermViewer() : window( initscr() ) {
  noecho();
  curs_set( 0 );
  // start_color();
  clear();
}

TermViewer::~TermViewer() {
  echo();
  curs_set( 1 );
  auto ret = endwin();
  std::cout << ret << "\n";
}

void TermViewer::_update( core::ImageView<core::ColorVec> const &img, size_t trows, size_t tcols ) {

  auto drow = img.rows / trows;
  auto dcol = img.cols / tcols;
  auto sq = static_cast<core::ColorVec::elem_type>( dcol * drow );

  std::array<core::ColorVec::elem_type, 3> rgb_agg{};
  core::ColorVec::elem_type mono = 0.0;
  core::ColorVec::elem_type scale = 2.0;
  char next = ' ';
  for ( size_t row = 0; row < trows; ++row ) {
    for ( size_t col = 0; col < tcols; ++col ) {

      // cache
      rgb_agg.fill( 0.0 );
      // aggregate
      for ( auto i = 0; i < drow - 1; ++i ) {
        for ( auto j = 0; j < dcol - 1; ++j ) {
          auto px = img.get_pixel( row * drow + i, col * dcol + j );
          for ( size_t n = 0; n < 3; ++n ) {
            rgb_agg[ n ] = rgb_agg[ n ] + px[ n ];
          }
        }
      }
      // normalize
      for ( auto n = 0; n < 3; ++n ) {
        rgb_agg[ n ] = rgb_agg[ n ] / sq;
      }
      mono = ( 0.2125 * rgb_agg[ 0 ] ) + ( 0.7154 * rgb_agg[ 1 ] ) + ( 0.0721 * rgb_agg[ 2 ] );
      mono = mono * scale * ( LUT.size() - 2 );
      mono = std::min( mono, static_cast<decltype( mono )>( LUT.size() - 2 ) );
      next = LUT.at( static_cast<size_t>( mono ) );
      // mvprintw(8, 5, "Last val: %4.2f -> %c", max_val, next);
      mvaddch( row, col, next );
    }
  }
  refresh();
}

void TermViewer::draw( core::ImageView<core::ColorVec> const &img ) {
  int tcols = -1;
  int trows = -1;
  int lastcols = -1;
  int lastrows = -1;
  int last_char = -1;

  // normalize img
  core::ColorVec::elem_type max_val = 0;
  for ( auto i = 0; i < img.size; ++i ) {
    auto rowcol = img.unravel_index( i );
    auto px = img.get_pixel( rowcol.first, rowcol.second );
    max_val = std::max( px[ 0 ], std::max( px[ 1 ], std::max( px[ 2 ], max_val ) ) );
  };
  for ( auto i = 0; i < img.size; ++i ) {
    auto rowcol = img.unravel_index( i );
    auto px = img.get_pixel( rowcol.first, rowcol.second );
    px[ 0 ] = px[ 0 ] / max_val;
    px[ 1 ] = px[ 1 ] / max_val;
    px[ 2 ] = px[ 2 ] / max_val;
  };

  while ( last_char != KEY_Q ) {
    // update termview dims
    getmaxyx( this->window, trows, tcols );
    if ( trows != lastrows || tcols != lastcols ) {
      this->_update( img, trows, tcols );
    }
    lastrows = trows;
    lastcols = tcols;
    last_char = getch();
    std::this_thread::sleep_for( std::chrono::milliseconds( 30 ) );
  }
}

int main() {
  std::cout << "HI!";
  core::Image<> img( 0, 0 );
  // img.load( "/home/rikisa/src/raytrace/res/test.png" );
  // img.load( "/home/rikisa/Downloads/test.png" );
  img.load( "/home/rikisa/Downloads/rose.png" );
  // img.load( "/home/rikisa/Documents/lineofsucc.png");
  TermViewer tvw{};
  tvw.draw( img );
  return 0;
}
