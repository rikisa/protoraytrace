#include "wrappedcont.hpp"

// impl
Deriv::Deriv( int c ) : c( c ) { this->a = this->a + c; };

Wrapper::Wrapper() : container( std::make_shared<Container>() ){};

template<class O>
void Container::add( O obj ) {
  this->objects.push_back( obj );
};

template<class O, typename... ArgsT>
void Wrapper::add( ArgsT... args ) {
  this->container->add<O>( O( args... ) );
};

int main( void ) {
  Wrapper wrp;

  wrp.add<Deriv, int>( 89 );

  std::cout << wrp.container->objects[ 0 ].a << " :)\n";

  return 0;
};
