#include "engine/renderobjects.hpp"
#include "engine/scene.hpp"

#include <array>
#include <iostream>
#include <string>

template<typename Rt, int ch>
void print_arr( std::array<Rt, ch> const &col ) {

  using namespace std;
  size_t size = col.size();
  cout << "Vec with " << size << " channels: (";
  for ( size_t i = 0; i < size - 1; ++i ) {
    cout << col[ i ] << ", ";
  }
  cout << col[ col.size() - 1 ] << ")\n";
}

NMArray<float, 3> get_one( void ) {
  NMArray<float, 3> one( 2 );
  one[ 0 ] = { 1, 2, 3 };
  one[ 1 ] = { 4, 5, 6 };
  return one;
}

int main( int argc, char *argv[] ) {
  NMArray<float, 3> one = get_one();

  NMArray<float, 3> arr1( 2 );
  arr1[ 0 ] = { 1, 0, 1 };
  arr1[ 1 ] = { 0, 1, 2 };

  NMArray<float, 3> arr0( 2 );
  arr0[ 0 ] = { -1, -1, -1 };
  arr0[ 1 ] = { -1, -1, -1 };

  auto arr31 = arr1 + arr0;
  print_arr<float, 3>( arr31[ 0 ] );
  print_arr<float, 3>( arr31[ 1 ] );

  arr0 = std::move( one );
  std::cout << "Len after move " << ( one.len ) << "\n";
  std::cout << "Size after move " << one.data.size() << "\n";

  auto arr3 = arr1 + arr0;

  print_arr<float, 3>( arr3[ 0 ] );
  print_arr<float, 3>( arr3[ 1 ] );

  // NMArray<float, 2> arr4( 2 );
  // auto arr5 = arr4 + arr0;
}
