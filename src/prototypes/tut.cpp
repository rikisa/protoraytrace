// not nessecarily needed, but i like to be explicit
extern "C" {
// this order to let GLFW know that we manage extension ourselfes
// but to be explicit... Allows to change order as we see fit
#define GLFW_INCLUDE_NONE
// #include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glad/glad.h>
}

#include "png++/image.hpp"

#include <iostream>
#include <memory>
#include <string>
#include <vector>

typedef unsigned int uint_t;

class TextureData {

public:
  std::vector<unsigned char> _data;
  size_t width;
  size_t height;
  const size_t channels = 3;

  ~TextureData() {}

  TextureData( std::string const &path ) : _data() {

    std::cout << "loading " << path << "\n";
    png::image<png::rgb_pixel> png( path );

    this->width = png.get_width();
    this->height = png.get_height();

    std::cout << "got W x H (" << this->width << ", " << this->height << ")\n";

    this->_data.reserve( this->width * this->height * 3 );
    for ( size_t row = 0; row < png.get_height(); ++row ) {
      auto cur_row = png.get_row( row );
      for ( size_t col = 0; col < png.get_width(); ++col ) {
        auto pixel = cur_row[ col ];
        this->_data.push_back( pixel.red );
        this->_data.push_back( pixel.green );
        this->_data.push_back( pixel.blue );
      }
    }

    std::cout << "got " << this->_data.size() << " values\n";
  } // TextureData

  void const *buf() { return static_cast<void *>( this->_data.data() ); }
};

// Events are handled by callbacks, which must be C or C++ static functions
// to be on the very save site, declare accordingly
extern "C" void error_callback( int error, char const *description ) {
  std::cout << "Error: " << description;
}

extern "C" void key_callback( GLFWwindow *window, int key, int scancode, int action, int mods ) {
  if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS ) {
    glfwSetWindowShouldClose( window, GLFW_TRUE );
  } else if ( key == GLFW_KEY_KP_ADD && action == GLFW_PRESS ) {
    int *zoom = (int *)glfwGetWindowUserPointer( window );
    *zoom = *zoom + 1;
    std::cout << "Zoom now " << *zoom << "\n";
  } else {
    std::cout << key << ", " << scancode << ", " << action << ", " << mods << "\n";
  }
}

extern "C" void fb_size_callback( GLFWwindow *window, int width, int height ) {
  glViewport( 0, 0, width, height );
}

int check_compile( unsigned int shader_object ) {
  // shader compiling error handling
  int success;
  char infoLog[ 512 ];
  glGetShaderiv( shader_object, GL_COMPILE_STATUS, &success );
  if ( !success ) {
    glGetShaderInfoLog( shader_object, 512, NULL, infoLog );
    std::cout << "SHADER COMPILE ERROR: " << infoLog << "\n";
  }
  return success;
}

class Viewer {
public:
  Viewer( uint_t widht, uint_t height );
};

int main( int argc, char **argv ) {

  if ( argc == 1 ) {
    std::cout << "Need path to texture image\n";
    return -1;
  }

  // set error callback, that allows us to catch errors
  // dureing initioalization
  glfwSetErrorCallback( error_callback );

  if ( !glfwInit() ) {
    // Initialization failed
    return -1;
  } else {
    std::cout << "OpenGL initialized\n";
  }

  // Opaque GLFWwindow can be wrapped if destructor call is passed as well
  // Handle for a window with some opengl context (state)
  std::shared_ptr<GLFWwindow> window(
      glfwCreateWindow( 640, 480, "My Title", NULL, NULL ), glfwDestroyWindow );
  // GLFWwindow* window = glfwCreateWindow(640, 480, "My Title", NULL, NULL);

  if ( !window ) {
    std::cout << "Window creatione failed\n";
    glfwTerminate(); // really needed?
    return -1;
  } else {
    std::cout << "Created window\n";
  }

  // must be done to select a opengl context to work with.
  // means, context creation attatched to window
  // creation does not make it current
  glfwMakeContextCurrent( window.get() );

  // GLEW
  // initialize extension loader
  // load entrypoints
  // if ( glewInit() != GLEW_OK ) {
  //     std::cout << "Could not load OpenGL entriepoints\n";
  // } else {
  //     std::cout << "OpenGL entrypoints loaded\n";
  // }

  // let GLAD work on everything GLFW has build up so far
  auto procAddr = (GLADloadproc)glfwGetProcAddress;
  if ( !gladLoadGLLoader( procAddr ) ) {
    std::cout << "Could not load OpenGL entry points\n";
    return -1;
  } else {
    std::cout << "OpenGL entry points loaded\n";
  }

  // get frambuffer size and set viewport
  int width, height;
  glfwGetFramebufferSize( window.get(), &width, &height );
  glViewport( 0, 0, width, height );

  glfwSetFramebufferSizeCallback( window.get(), fb_size_callback );
  glfwSetKeyCallback( window.get(), key_callback );

  // Only swap gf/bg buffers after waiting for full screen update
  // vsync
  glfwSwapInterval( 1 );

  // clear color buffer do nice choosen RGBa color
  glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
  glClear(
      GL_COLOR_BUFFER_BIT ); //  GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT and GL_STENCIL_BUFFER_BIT

  // triangle vertices
  std::vector<float> object_data = {
      // positions          // colors           // texture coords
      0.5f,  0.5f,  0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top right
      0.5f,  -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // bottom right
      -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom left
      -0.5f, 0.5f,  0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f  // top left
  };
  std::vector<unsigned int> indices = {
      // note that we start from 0!
      0, 1, 3, // first triangle
      1, 2, 3  // second triangle
  };

  /* OpenGL starts here
   */

  // bind all stuff follwoing to Vertex array object
  unsigned int VAO;
  glGenVertexArrays( 1, &VAO );
  glBindVertexArray( VAO );

  // create buffer object and bind to specific (unique) buffer type
  // here array buffere aka vertex buffer
  // binding -> reference buffer we operate on
  unsigned int ABO;
  glGenBuffers( 1, &ABO );
  glBindBuffer( GL_ARRAY_BUFFER, ABO );
  glBufferData(
      GL_ARRAY_BUFFER, sizeof( object_data[ 0 ] ) * object_data.size(), object_data.data(),
      GL_STATIC_DRAW // affects mem allocaition on GPU
                     // static: changed once, read often; dynamic + changed often
  );

  // Element buffser for aggregating shared vertices into elements
  // Needs a bound array buffer
  unsigned int EBO;
  glGenBuffers( 1, &EBO );
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, EBO );
  glBufferData(
      GL_ELEMENT_ARRAY_BUFFER, sizeof( indices[ 0 ] ) * indices.size(), indices.data(),
      GL_STATIC_DRAW );

  // tell opengl how to interpret data in vertex buffer
  // will always refer to current vertex buffer
  // will be bound to current VAO
  glVertexAttribPointer(
      0,                              // index for this attribute
      3,                              // elements per vertice
      GL_FLOAT,                       // type
      GL_FALSE,                       // normalize/clamp to -1,1 / 0,1
      8 * sizeof( object_data[ 0 ] ), // number of bytes per vertex attribute
                                      // can be deduced from type and
                                      // size param, if tightly packed
      (void *)0                       // void pointer to first element
  );

  // vertives color
  glVertexAttribPointer(
      1,                                         // index for this attribute
      3,                                         // elements per vertice
      GL_FLOAT,                                  // type
      GL_FALSE,                                  // normalize/clamp to -1,1 / 0,1
      8 * sizeof( object_data[ 0 ] ),            // number of bytes per vertex
                                                 // can be deduced from type and
                                                 // size param, if tightly packed
      (void *)( 3 * sizeof( object_data[ 0 ] ) ) // void pointer to first element
  );

  // vertices texture mapping
  glVertexAttribPointer(
      2,                                         // index for this attribute
      2,                                         // elements per vertice
      GL_FLOAT,                                  // type
      GL_FALSE,                                  // normalize/clamp to -1,1 / 0,1
      8 * sizeof( object_data[ 0 ] ),            // number of bytes per vertex
                                                 // can be deduced from type and
                                                 // size param, if tightly packed
      (void *)( 6 * sizeof( object_data[ 0 ] ) ) // void pointer to first element
  );

  glEnableVertexAttribArray( 0 );
  glEnableVertexAttribArray( 1 );
  glEnableVertexAttribArray( 2 );

  std::string vertex_shader_src = "#version 330 core\n"
                                  "layout (location = 0) in vec3 aPos;\n"
                                  "layout (location = 1) in vec3 aColor;\n"
                                  "layout (location = 2) in vec2 aTexCoord;\n"
                                  "out vec3 ourColor;\n"
                                  "out vec2 TexCoord;\n"
                                  "void main()\n"
                                  "{\n"
                                  "   gl_Position = vec4(aPos, 1.0);\n"
                                  "   ourColor = aColor;\n"
                                  "   TexCoord = aTexCoord;\n"
                                  "}\0";
  char const *vertex_shader_src_char = vertex_shader_src.c_str();
  unsigned int VSO;
  VSO = glCreateShader( GL_VERTEX_SHADER );
  // expects a list of strings aka char**, stringlenght := NULL
  glShaderSource( VSO, 1, &vertex_shader_src_char, NULL );
  glCompileShader( VSO );

  std::string fragment_shader_src =
      "#version 330 core\n"
      "out vec4 FragColor;\n"
      "in vec3 ourColor;\n"
      "in vec2 TexCoord;\n"
      "uniform sampler2D ourTexture;\n"
      "void main()\n"
      "{\n"
      "    // FragColor = vec4(ourColor, 1.0f);\n"
      "    FragColor = texture(ourTexture, TexCoord) * pow(TexCoord.x - TexCoord.y, 2);\n"
      "    // FragColor = vec4(ourColor.x, ourColor.y, ourColor.z, 1.0f);\n"
      "} \n";
  char const *frag_shader_src_char = fragment_shader_src.c_str();
  unsigned int FSO;
  FSO = glCreateShader( GL_FRAGMENT_SHADER );
  // expects a list of strings aka char**, stringlenght := NULL
  glShaderSource( FSO, 1, &frag_shader_src_char, NULL );
  glCompileShader( FSO );

  check_compile( VSO );
  check_compile( FSO );

  // build shader program
  // and link shaders
  unsigned int shaderProgram;
  shaderProgram = glCreateProgram();
  glAttachShader( shaderProgram, VSO );
  glAttachShader( shaderProgram, FSO );
  glLinkProgram( shaderProgram );
  // use it...!
  // clean up the (static) linked shaders. they live in the programm now
  glDeleteShader( VSO );
  glDeleteShader( FSO );
  // glGetProgrammiv, get log and success... omitted

  // Texture fun!
  auto texdat = TextureData( argv[ 1 ] );
  std::cout << "loaded image texture!\n";
  unsigned int texId;
  glGenTextures( 1, &texId );
  glActiveTexture( GL_TEXTURE0 ); // activating texture unit to bind to
  glBindTexture( GL_TEXTURE_2D, texId );
  // operate on binded 2d texture, but not needed for now
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGB, texdat.width, texdat.height, 0, GL_RGB, GL_UNSIGNED_BYTE,
      texdat.buf() );

  std::cout << "created texture from" << argv[ 1 ] << "\n";

  // not needed for image viewer ;)
  // glGenerateMipmap( GL_TEXTURE_2D );
  // delete texdat;

  // make pointer to userdata, so that it is available in
  // e.g. callbacks, which pass GLFWWindow pointer
  int zoom = 0;
  glfwSetWindowUserPointer( window.get(), static_cast<void *>( &zoom ) );

  glUseProgram( shaderProgram );
  std::cout << "enter render loop"
            << "\n";
  while ( !glfwWindowShouldClose( window.get() ) ) {

    // glDrawArrays( GL_LINE_LOOP, 0, 3 );

    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

    glfwSwapBuffers( window.get() );
    glfwPollEvents();
  }

  std::cout << "Pa-pa\n";

  // glfwTerminate is cranky, if there is no context left...
  // We do not want cranky error messages at the end.
  glfwSetErrorCallback( NULL );
  glfwTerminate();
  return 0;
}
