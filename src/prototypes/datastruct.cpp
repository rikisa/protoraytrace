/* Testing / timing for which data struct to use
 */

#include <array>
#include <iostream>
#include <exception>
#include <string>
#include <vector>


template<int CH>
std::vector<std::array<float, CH>> vec_of_arr ( int size, int steps ) {
    std::vector<std::array<float, CH>> vec( size );

    unsigned long idx;
    float rx;
    float ri;
    for ( int i = 0; i < steps; ++i ) {
        idx = i % size;
        rx = (float) idx;
        ri = (float) i;
        std::array<float, CH>& cur_arr = vec[idx];
        cur_arr[0] = ri;
        cur_arr[1] = rx;
        cur_arr[2] = rx + ri;
        // std::cout << "idx: " << idx << " ("
        //           << cur_arr[0] << ", "
        //           << cur_arr[1] << ", "
        //           << cur_arr[2] << ")\n";

    }

    return vec;
};

template<int CH>
std::vector<float> vec_only ( int size, int steps ) {
    std::vector<float> vec( size * CH );

    unsigned long idx;
    unsigned long c_idx;
    float rx;
    float ri;

    for ( int i = 0; i < steps; ++i ) {
        idx = i % size;
        rx = (float) idx;
        ri = (float) i;

        c_idx = idx * CH;
        vec[c_idx] = ri;
        vec[++c_idx] = rx;
        vec[++c_idx] = rx + ri;

        // std::cout << "idx: " << idx << " ("
        //           << vec[c_idx-2] << ", "
        //           << vec[c_idx-1] << ", "
        //           << vec[c_idx] << ")\n";
    }

    return vec;
};

template<int CH>
std::vector<std::vector<float>> vec_of_vec ( int size, int steps ) {
    std::vector<std::vector<float>> vec( size );

    unsigned long idx;
    unsigned long c_idx;
    float rx;
    float ri;

    for ( int i = 0; i < steps; ++i ) {
        idx = i % size;
        rx = (float) idx;
        ri = (float) i;
        std::vector<float>& cur_arr = vec[idx];
        cur_arr = {ri, rx, rx + ri};
    }
    return vec;
};

int main ( void ) {
    // std::cout << argv[0] << ": ";
    // if ( argc != 3) {
    //     // throw std::invalid_argument("Must provide exactly 2 values!");
    //     std::cout << "Must provide exactly 2 values!\n";
    //     return 1;
    // }
    // int size = std::stoi(argv[1]);
    // int steps = std::stoi(argv[2]);
    // std::cout << "running " << size << " for " << steps << " steps\n";

    int size = 25000000;
    int steps = 1000;
#ifdef VecOfArr
#pragma message("Vec Of Arr")
    vec_of_arr( size, steps );
#endif
#ifdef VecOnly
#pragma message("Vec Only")
    vec_only( size, steps );
#endif
#ifdef VecOfVec
#pragma message("Vec Of Vec")
    vec_of_vec( size, steps );
#endif
#ifdef Cmp

    auto &&voa = vec_of_arr<4>(10, 50);
    auto &&vo = vec_only<4>(10, 50);
    auto &&vov = vec_of_vec<4>(10, 50);

    std::vector<std::array<float, 4>> vcp( voa );

    std::cout << "\tVoA\tVO\tVoV\tVoC\n";
    for (int i = 0; i < 3; ++i) {
        std::cout << i
                  << "\t" << voa[9][i]
                  << "\t" << vo[(9 * 4) + i]
                  << "\t" << vov[9][i]
                  << "\t" << vcp[9][i]
                  << "\n";
    }

#endif
    std::cout << "done\n";
    return 0;
}

