local width = 640
local height = 480

math.randomseed(os.time())

local img = Image.new(width, height)
show( img )

function rect(img, col, row, rec_w, rgb)
    for r = row, row+rec_w-1 do
        for c = col, col+rec_w-1 do
            img:set_pixel(c, r, rgb)
        end
    end
end

function round(x)
    if (x - math.floor(x)) > 0.5 then
        return math.ceil(x)
    else
        return math.floor(x)
    end
end

function paint(img, col, row, rec_w, rgb)
    local col = round(col)
    local row = round(row)
    for r = row, row+rec_w-1 do
        for c = col, col+rec_w-1 do
            -- cur_rgb = {}
            -- dist = math.sqrt((row-r)^2 + (col-c)^2)
            -- for i, v in ipairs(rgb) do
                -- cur_rgb[i] = v * (1 / (1+dist))
            -- end
            img:set_pixel(c, r, rgb)
        end
    end
end

-- mark left/right edge
for r = 0, height-1 do
    img:set_pixel(0, r, {1, 0, 0})
    img:set_pixel(width-1, r, {0, 0, 1})
end

-- mark top/bottom edge
for c = 0, width-1 do
    img:set_pixel(c, 0, {0, 1, 0})
    img:set_pixel(c, height-1, {math.random(), math.random(), math.random()})
end

-- mark the corners
local rec_size = 5
for i, hor in ipairs({0, width-rec_size}) do
    for j, vert in ipairs({0, height-rec_size}) do
        rect(img, hor, vert, rec_size, {1, 1, 1})
    end
end

local periods = 10
local samples = 2000
-- init first value
local last_x = 0
local last_y = math.sin(0) * (height * 0.4) + (height * 0.5)
local rgb = {0.5, 0.5, 0.5}
local d_rgb = 0.01
for n = 1, samples do
    -- next point
    local _x = ((width-5) / samples) * n
    local xrad = _x / (width-5) * math.pi * 2 * periods
    local _y = math.sin(xrad) * (height * 0.4) + (height * 0.5)
    local next_x = _x
    local next_y = _y

    -- interpolate
    local start_y = 0
    local stop_y = 0
    for cur_x = last_x, next_x do
        if last_y > next_y then
            start_y, stop_y = next_y, last_y
        else
            start_y, stop_y = last_y, next_y
        end
        for cur_y = round(start_y), round(stop_y) do
            paint(img, cur_x, cur_y, 3, rgb)
        end
    end
    last_x = next_x
    last_y = next_y
    for i, v in ipairs(rgb) do
        rgb[i] = math.min(math.max(v + d_rgb * (1 - 2 * round(math.random())), 0), 1)
    end
end

img:save("sin.png")
