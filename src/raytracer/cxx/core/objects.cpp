#include "core/objects.hpp"

#include <array>
#include <iostream>
#include <utility>

template<class Color>
Material<Color>::Material() : color( 0.0 ){};
template<class Color>
Material<Color>::Material( Color color ) : color( color ){};

template<class Color>
Material<Color>::Material( typename Color::array_type color ) : color( color ){};

template<class Color, class BaseVec>
Traceable<Color, BaseVec>::Traceable(
    Material<Color> mat, BaseVec pos, core::CoordBase<BaseVec> ori )
    : mat( mat ), pos( pos ), ori( ori ){};
template<class Color, class BaseVec>
Traceable<Color, BaseVec>::Traceable( Material<Color> mat, BaseVec pos ) : mat( mat ), pos( pos ){};
template<class Color, class BaseVec>
Traceable<Color, BaseVec>::Traceable( Material<Color> mat ) : mat( mat ), pos( { 0, 0, 0 } ){};

template<class Color, class BaseVec>
auto Traceable<Color, BaseVec>::intersect( core::Ray<Color, BaseVec> /*unused*/ ) const -> float {
  std::cout << "Intersect Traceable\n";
  return 0;
}

template<class Color, class BaseVec>
Sphere<Color, BaseVec>::Sphere( float radius, Material<Color> mat, BaseVec pos )
    : Traceable<Color, BaseVec>( mat, pos ), radius( radius ){};

template<class Color, class BaseVec>
Sphere<Color, BaseVec>::Sphere( float radius, Material<Color> mat )
    : Traceable<Color, BaseVec>( mat ), radius( radius ){};

// auto Sphere::intersect( Ray ) -> float { return 0; };

template<class Color, class BaseVec>
auto Sphere<Color, BaseVec>::intersect( core::Ray<Color, BaseVec> /*unused*/ ) const -> float {
  std::cout << "Intersect Sphere\n"
            << "RGB(" << this->mat.color[ 0 ] << ", " << this->mat.color[ 1 ] << ", "
            << this->mat.color[ 2 ] << ")\n\n";
  return 1;
}

template<class Color, class BaseVec>
void Sphere<Color, BaseVec>::set_material( Material<Color> mat ) {
  this->mat = mat;
}

template<class Color, class BaseVec>
template<class Obj>
void Scene<Color, BaseVec>::add_object( Obj object ) {
  this->objects.emplace_back( std::make_shared<Obj>( object ) );
};

template<class Color, class BaseVec>
void Scene<Color, BaseVec>::intersect( core::Ray<Color, BaseVec> ray ) const {
  for ( auto obj = this->objects.cbegin(); obj != this->objects.end(); ++obj ) {
    obj->get()->intersect( ray );
  }
}

template struct Material<core::ColorVec>;
template class Traceable<core::ColorVec, core::PosVec>;
template class Sphere<core::ColorVec, core::PosVec>;

template void
    Scene<core::ColorVec, core::PosVec>::add_object( Sphere<core::ColorVec, core::PosVec> );
template void
    Scene<core::ColorVec, core::PosVec>::intersect( core::Ray<core::ColorVec, core::PosVec> ) const;
