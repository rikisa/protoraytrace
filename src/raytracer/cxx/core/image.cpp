#include "core/image.hpp"

#include <cstddef>
#include <fmt/core.h>
#include <iostream>
#include <png++/png.hpp>
#include <string>
#include <utility>

namespace core {

template<class Color>
ImageView<Color>::ImageView(
    size_t rows, size_t cols, std::shared_ptr<typename ImageView<Color>::container_t> data )
    : rows( rows ), cols( cols ), size( rows * cols ), data( std::move( data ) ) {}

template<class Color>
auto ImageView<Color>::ravel_index( size_t row, size_t col ) const noexcept -> size_t {
  auto index = ( this->cols * row + col ) * Color::dim;
  return index;
}

template<class Color>
auto ImageView<Color>::unravel_index( size_t index ) const noexcept -> std::pair<size_t, size_t> {
  return std::pair<size_t, size_t>{ index / this->cols /*col*/, index % this->cols /*row*/ };
}

template<class Color>
void ImageView<Color>::save( std::string const &filename ) {
  // image( width, height )
  constexpr typename Color::elem_type MAXRGB = 255.0;
  png::image<png::rgb_pixel> image( this->cols, this->rows );
  for ( size_t row = 0; row < this->rows; ++row ) {
    for ( size_t col = 0; col < this->cols; ++col ) {
      auto index = this->ravel_index( row, col );
      auto red = static_cast<int>( this->data->at( index ) * MAXRGB );
      auto green = static_cast<int>( this->data->at( index + 1 ) * MAXRGB );
      auto blue = static_cast<int>( this->data->at( index + 2 ) * MAXRGB );
      image[ row ][ col ] = png::rgb_pixel( red, green, blue );
    }
  }
  // std::cout << fmt::format(
  //     "Image({}, {}) PNG({}, {})\n", this->cols, this->rows, image.get_width(),
  //     image.get_height() );
  image.write( filename );
}

template<class Color>
void Image<Color>::load( std::string const &filename ) {
  // image( width, height )
  constexpr typename Color::elem_type MAXRGB = 255.0;
  png::image<png::rgb_pixel> const img_file( filename );
  auto rows = img_file.get_height();
  auto cols = img_file.get_width();
  this->resize( rows, cols );

  for ( size_t row = 0; row < this->rows; ++row ) {
    for ( size_t col = 0; col < this->cols; ++col ) {
      auto pixel = img_file.get_pixel( col, row );
      auto index = this->ravel_index( row, col );
      this->data->at( index ) = static_cast<typename Color::elem_type>( pixel.red ) / MAXRGB;
      this->data->at( index + 1 ) = static_cast<typename Color::elem_type>( pixel.green ) / MAXRGB;
      this->data->at( index + 2 ) = static_cast<typename Color::elem_type>( pixel.blue ) / MAXRGB;
      // std::cout << fmt::format("{} -> {} {} {}\n",
      //     index, this->data->at( index ),
      //     this->data->at( index + 1 ),
      //     this->data->at( index + 2 ) );
    }
  }
}

template<class Color>
auto ImageView<Color>::rgba8_begin() const -> rgba8_iterator {
  return ImageView<Color>::rgba8_iterator( this->data );
};

template<class Color>
auto ImageView<Color>::rgba8_end() const -> rgba8_iterator {
  return ImageView<Color>::rgba8_iterator( this->data, this->size );
};

template<class Color>
void ImageView<Color>::resize( size_t rows, size_t cols ) {
  this->rows = rows;
  this->cols = cols;
  this->size = rows * cols;
  this->data->resize( rows * cols * Color::format );
}

template<class Color>
auto ImageView<Color>::get_channel( rt_index row, rt_index col, rt_index chan ) const ->
    typename Color::elem_type {
  return this->data->at( this->ravel_index( row, col ) + chan );
}

template<class Color>
auto ImageView<Color>::get_pixel( rt_index row, rt_index col ) const -> Color {
  auto rgb0 = this->data->data() + this->ravel_index( row, col );
  if ( Color::dim == 3 ) {
    return Color( { *( rgb0++ ), *( rgb0++ ), *( rgb0++ ) } );
  }
}

template<class Color>
Image<Color>::Image( size_t rows, size_t cols )
    : ImageView<Color>( rows, cols, std::make_shared<typename ImageView<Color>::container_t>() ) {
  this->resize( rows, cols );
};

template<class Color>
auto Image<Color>::get_view() -> ImageView<Color> {
  return ImageView<Color>( this->rows, this->cols, this->data );
};

template<class Color>
void Image<Color>::set_pixel( rt_index row, rt_index col, Color const &rgb ) {
  auto offset = this->ravel_index( row, col );
  // this->data->data()[std::slice(offset, rgb.format, 1)];
  // (*dat)[std::slice(offset, rgb.format, 1)];
  for ( size_t i = 0; i < Color::dim; ++i ) {
    this->data->data()[ offset + i ] = rgb[ i ];
  }
}

template<class Color>
auto get_empty_view() -> ImageView<Color> {
  auto _temp = Image<Color>( 1, 1 );
  return _temp.get_view();
}

template<class Color>
ImageView<Color>::rgba8_iterator::rgba8_iterator( std::shared_ptr<container_t> data, size_t offset )
    : m_data( std::move( data ) ), m_cur_pixel( offset ) {
  this->set_data();
};

template<class Color>
void ImageView<Color>::rgba8_iterator::set_data() {
  for ( auto i = 0; i < 3; ++i ) {
    auto rgb_val = this->m_data->at( this->m_at_channel + i );
    this->m_cur_rgba8.at( i ) = static_cast<std::uint8_t>( rgb_val * this->MAX_VAL );
  }
  this->m_cur_rgba8.at( 4 ) = this->MAX_VAL;
}

template<class Color>
auto ImageView<Color>::rgba8_iterator::operator*() -> std::uint8_t & {
  return this->m_cur_rgba8.at( this->m_cur_channel );
};

template<class Color>
auto ImageView<Color>::rgba8_iterator::operator->() -> std::uint8_t * {
  return &( this->m_cur_rgba8.at( this->m_cur_channel ) );
};

template<class Color>
void ImageView<Color>::rgba8_iterator::increment_channel() {
  auto index = this->m_cur_channel + this->m_cur_pixel + 1;
  this->m_cur_pixel = index / 4;
  this->m_cur_channel = index % 4;
};

template<class Color>
auto ImageView<Color>::rgba8_iterator::operator++() -> ImageView<Color>::rgba8_iterator & {
  this->increment_channel();
  return *this;
};

template<class Color>
auto ImageView<Color>::rgba8_iterator::operator++( int ) -> ImageView<Color>::rgba8_iterator {
  auto prev = rgba8_iterator( this->m_data );
  prev.m_at_channel = this->m_at_channel;
  this->increment_channel();
  return prev;
}

template<class Color>
void ImageView<Color>::rgba8_iterator::operator+=( size_t const &offset ) {
  this->m_cur_channel += offset;
};

template<class Color>
auto ImageView<Color>::rgba8_iterator::operator==( rgba8_iterator const &other ) const -> bool {
  return this->m_cur_channel == other.m_cur_channel;
};

template<class Color>
auto ImageView<Color>::rgba8_iterator::operator!=( rgba8_iterator const &other ) const -> bool {
  return !( this->m_cur_channel == other.m_cur_channel );
};

template class Image<ColorVec>;
template class ImageView<ColorVec>;
template ImageView<ColorVec> get_empty_view<ColorVec>();
} // namespace core
