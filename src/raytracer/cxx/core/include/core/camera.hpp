/* Everything rendering e.g. the Scene which contains objects and ray, which
 * interacts with the objects in a scene, or the camera which interacts with
 * the ray
 */
#pragma once

#include "core/typedefs.hpp"
#include "image.hpp"
#include "linalg.hpp"

#include <iterator>
#include <memory>
#include <string>
#include <tuple>
#include <typeinfo>
#include <vector>

namespace core {

template<class Color>
class Image;

template<class Color, class BaseVec>
struct Ray {

public:
  // origin in image plane
  BaseVec origin;
  // current base
  BaseVec base;
  BaseVec dir;

  Ray();
  Ray( BaseVec const & /*origin*/, BaseVec const & /*point_a*/, BaseVec const & /*point_b*/ );
  void
  move_to( BaseVec const & /*new_base*/, BaseVec const & /*dir_from*/, BaseVec const & /*dir_to*/ );
  void move_to( BaseVec const & /*new_base*/ );

  void sample( ColorVec const & /*color*/ );
  [[nodiscard]] auto color() const -> Color;

private:
  Color m_color = { { 0, 0, 0 } };
  size_t m_samples = 0;
  static constexpr size_t _m_min_sample = 1;
};

// rays always shooting in z direction. y dim is aligned with image rows,
// x dim is aligned with image cols
// An image alwas has the coord size img_scal
// CoordBase is in center of image!
template<class Color, class BaseVec>
class Camera {

protected:
  // ray iterator
  struct iterator;

public:
  explicit Camera(
      Image<Color> & /*image*/, typename BaseVec::elem_type /*image_width*/ = 1,
      CoordBase<BaseVec> const & /*orientation*/ = CoordBase<BaseVec>(),
      typename BaseVec::elem_type /*focal_len*/ = 1 );

  [[nodiscard]] auto orientation() const -> CoordBase<BaseVec> const *;

  void look_at( BaseVec /*poi*/ );

  void apply_ray( rt_index /*row*/, rt_index /*col*/, Ray<Color, BaseVec> const & /*ray*/ );

  // getters
  auto focus() -> BaseVec const &;
  auto image() -> Image<Color> const &;

  auto begin() -> iterator;
  auto end() -> iterator;

private:
  Image<Color> &m_image;
  std::unique_ptr<CoordBase<BaseVec>> m_orientation;
  typename BaseVec::elem_type m_focal_len;
  typename BaseVec::elem_type m_img_width;
  BaseVec m_focus;

  void update_focal_point();
};

// Iterator in Camera Namespace
// thats has creates and modifies the camera Ray
// 'Iterating Interface' to camera and ray
template<class Color, class BaseVec>
struct Camera<Color, BaseVec>::iterator {

  using iterator_category = std::input_iterator_tag;
  using value_type = Ray<Color, BaseVec>;
  using pointer = Ray<Color, BaseVec> *;
  using reference = Ray<Color, BaseVec> &;

  explicit iterator( Camera<Color, BaseVec> & /*camera*/, size_t /*start*/ = 0 );
  ~iterator() = default;

  iterator() = delete;
  iterator( iterator & ) = delete;
  auto operator=( iterator const & ) -> iterator & = delete;
  auto operator=( iterator const && ) -> iterator & = delete;

  auto operator*() -> Ray<Color, BaseVec> &;
  auto operator->() -> Ray<Color, BaseVec> *;
  auto operator++() -> iterator &;
  auto operator++( int ) -> iterator;

  void operator+=( size_t const & /*offset*/ );
  auto operator==( iterator const & /*other*/ ) const -> bool;
  auto operator!=( iterator const & /*other*/ ) const -> bool;

  void apply_current_ray() const;

private:
  // for postfix increment, move out result of calculation
  iterator( iterator && ) noexcept = default;

  using coord_t = CoordBase<BaseVec>;
  using scalar_t = typename BaseVec::elem_type;

  Camera &m_camera;
  Ray<Color, BaseVec> m_ray;

  size_t m_current_row{};
  size_t m_current_col{};

  BaseVec m_col_step;
  BaseVec m_row_step;
  BaseVec m_top_left;

  /* reset ray base and origin according to curren row and col */
  inline void reset_ray() noexcept;
  /* updating cur row and col from index, then calling reset_ray()*/
  inline void reset_ray( size_t /*index*/ ) noexcept;
  /* updating cur row and col, then calling reset_ray()*/
  inline void reset_ray( size_t /*row*/, size_t /*col*/ ) noexcept;
  /* increment curretn ray by one step */
  inline void increment_ray() noexcept;

}; // iterator
} // namespace core
