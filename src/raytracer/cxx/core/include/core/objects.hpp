/* All objects, lights, materials which are in a scene and can be part of the scene
 */
#pragma once

#include "camera.hpp"
#include "linalg.hpp"
#include "objects.hpp"

#include <string>

using std::string;

template<class Color>
struct Material {

  Color color;

  Material();
  explicit Material( Color /*color*/ );
  explicit Material( typename Color::array_type /*color*/ );
};

template<class Color, class BaseVec>
class Traceable {

protected:
  explicit Traceable( Material<Color> /*mat*/ );
  Traceable( Material<Color> /*mat*/, BaseVec /*pos*/ );
  Traceable( Material<Color> /*mat*/, BaseVec /*pos*/, core::CoordBase<BaseVec> ori );
  Traceable( Traceable const & ) = default;

public:
  virtual ~Traceable() = default;

  Material<Color> mat;
  BaseVec pos;
  core::CoordBase<BaseVec> ori;

  [[nodiscard]] virtual auto intersect( core::Ray<Color, BaseVec> /*unused*/ ) const -> float = 0;
};

template<class Color, class BaseVec>
class Sphere : public Traceable<Color, BaseVec> {

protected:
  float radius;

public:
  Sphere( float /*radius*/, Material<Color> /*mat*/, BaseVec /*pos*/ );
  Sphere( float /*radius*/, Material<Color> /*mat*/ );
  void set_material( Material<Color> /*mat*/ );

  // auto intersect( Ray ) -> float;
  [[nodiscard]] auto intersect( core::Ray<Color, BaseVec> /*unused*/ ) const -> float override;
};

template<class Color, class BaseVec>
class Scene {

public:
  using container_t = std::vector<std::shared_ptr<Traceable<Color, BaseVec>>>;
  /**
   * @brief Add an existing Tracabel
   * object instance
   *
   * @tparam Obj Sublass of Tracable
   * @param object Instance to add
   */
  template<class Obj>
  void add_object( Obj object );

  void intersect( core::Ray<Color, BaseVec> /*ray*/ ) const;

  auto begin() -> typename container_t::iterator;
  auto end() -> typename container_t::iterator const;

private:
  container_t objects;
};
