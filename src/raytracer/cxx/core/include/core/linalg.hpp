#pragma once

#include "core/typedefs.hpp"

#include <array>
#include <cstddef>
#include <initializer_list>
#include <iostream>

namespace core {

enum ColorFormat : size_t { RGB = 3, RGBA = 4 };

template<typename T, size_t n>
class ArrayN {
public:
  using elem_type = T;
  using array_type = std::array<T, n>;
  using iterator = typename std::array<T, n>::iterator;
  using const_iterator = typename std::array<T, n>::const_iterator;

  explicit ArrayN() = default;
  explicit ArrayN( T const & /*_val*/ );
  // ArrayN( std::array<T, n>  );
  explicit ArrayN( std::array<T, n> const & /*values*/ );
  // ArrayN( std::initializer_list<T> const );

  auto operator[]( rt_index /*index*/ ) const -> T;
  auto operator[]( rt_index /*index*/ ) -> T &;
  void operator=( typename ArrayN<T, n>::array_type const
                      & ); // NOLINT Updating / setting variables from {...} inplace
  void operator-=( ArrayN<T, n> const & /*other*/ );
  void operator-=( T const & /*scalar*/ );
  void operator+=( ArrayN<T, n> const & /*other*/ );
  void operator+=( T const & /*scalar*/ );
  void operator*=( ArrayN<T, n> const & /*other*/ );
  void operator*=( T const & /*scalar*/ );
  void operator/=( ArrayN<T, n> const & /*other*/ );
  void operator/=( T const & /*scalar*/ );

  // infix
  auto operator-( ArrayN<T, n> const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator-( T const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator+( ArrayN<T, n> const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator+( T const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator*( ArrayN<T, n> const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator*( T const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator/( ArrayN<T, n> const & /*other*/ ) const -> ArrayN<T, n>;
  auto operator/( T const & /*other*/ ) const -> ArrayN<T, n>;

  auto operator==( ArrayN<T, n> const & /*other*/ ) const -> bool;
  auto operator!=( ArrayN<T, n> const & /*other*/ ) const -> bool;

  [[nodiscard]] auto cget() const -> std::array<T, n> const &;

  // conversions
  explicit operator array<T, n>() const;
  explicit operator array<T, n> const() const;

  static constexpr size_t dim = n;

  auto begin() -> iterator;
  auto end() -> iterator;

  [[nodiscard]] auto cbegin() const -> const_iterator;
  [[nodiscard]] auto cend() const -> const_iterator;

  // inplace normalize to L2 norm,
  // that eucledian length is 1
  void normalize();

  // the L2 norm
  [[nodiscard]] auto norm() const -> T;

  // element wise sum
  [[nodiscard]] auto sum() const -> T;

  // dot product
  [[nodiscard]] auto dot( ArrayN<T, n> const & /*other*/ ) const -> T;

  // cosine of angle between two vectors
  [[nodiscard]] auto cos( ArrayN<T, n> const & /*other*/ ) const -> T;

  friend auto operator<<( std::ostream &stream, ArrayN<T, n> const & /*arr*/ ) -> std::ostream &;

protected:
  auto get() -> std::array<T, n> &;

private:
  array<T, n> m_values;
};

/* Matrix class for object coordinate base
 * Defines Orientation of objects in space
 */
template<class SpatialVector>
class CoordBase {
public:
  SpatialVector ydir; // rows, 1st dim
  SpatialVector xdir; // cols, 2nd dim
  SpatialVector zdir; // depth, 3rd dim
  SpatialVector pos;  // position of origin

  explicit CoordBase( SpatialVector const &pos = SpatialVector( 0 ) );
  explicit CoordBase(
      SpatialVector const & /*up*/, SpatialVector const & /*forward*/,
      SpatialVector const & /*pos*/ = SpatialVector( 0 ) );

  using elem_type = SpatialVector;

private:
  void normalize();
};

/* Array class for color space
 * implements all functions for
 * color manipulation
 */
template<typename T, ColorFormat mode>
class ColorVector : public ArrayN<T, static_cast<size_t>( mode )> {
public:
  using ArrayN<T, static_cast<size_t>( mode )>::ArrayN;

  explicit ColorVector( ArrayN<T, static_cast<size_t>( mode )> const & /*arr*/ );

  static constexpr ColorFormat format = mode;

  auto blend( ColorVector const &other ) -> ColorVector;
};

template<typename T>
auto cross( ArrayN<T, 3> const & /*some*/, ArrayN<T, 3> const & /*other*/ ) -> ArrayN<T, 3>;

using FArray3 = ArrayN<float, 3>;
using FArray4 = ArrayN<float, 4>;
using DArray3 = ArrayN<double, 3>;
using DArray4 = ArrayN<double, 4>;

// default values through-out the the raytracer
using PosVec = ArrayN<rt_float, 3>;
using ColorVec = ColorVector<rt_float, ColorFormat::RGB>;

// Printing vecs
auto operator<<( std::ostream &stream, FArray3 const &arr ) -> std::ostream &;
auto operator<<( std::ostream &stream, FArray4 const &arr ) -> std::ostream &;
auto operator<<( std::ostream &stream, DArray3 const &arr ) -> std::ostream &;
auto operator<<( std::ostream &stream, DArray4 const &arr ) -> std::ostream &;

// infix operators
template<typename T, size_t n>
auto operator-( T const & /*scalar*/, ArrayN<T, n> const & /*arr*/ ) -> ArrayN<T, n>;
template<typename T, size_t n>
auto operator+( T const & /*scalar*/, ArrayN<T, n> const & /*arr*/ ) -> ArrayN<T, n>;
template<typename T, size_t n>
auto operator*( T const & /*scalar*/, ArrayN<T, n> const & /*arr*/ ) -> ArrayN<T, n>;
template<typename T, size_t n>
auto operator/( T const & /*scalar*/, ArrayN<T, n> const & /*arr*/ ) -> ArrayN<T, n>;

} // namespace core
