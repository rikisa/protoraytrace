#pragma once

#include "core/image.hpp"
#include "linalg.hpp"
#include "util.hpp"

#include <iostream>

template<typename T = core::rt_float>
struct divmod {
public:
  divmod( size_t /*number*/, size_t /*quot*/ );
  T const div;
  T const rem;
};
