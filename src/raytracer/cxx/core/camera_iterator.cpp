#include "core/camera.hpp"
#include "core/util.hpp"

#include <cstddef>
#include <iostream>

namespace core {
template<class Color, class BaseVec>
Camera<Color, BaseVec>::iterator::iterator( Camera<Color, BaseVec> &camera, size_t start )
    : m_camera( camera ) {

  auto frows = static_cast<typename BaseVec::elem_type>( this->m_camera.m_image.rows );
  auto fcols = static_cast<typename BaseVec::elem_type>( this->m_camera.m_image.cols );

  // as aspect is given from image specs, pixel size can be calculated directly
  auto image_width = this->m_camera.m_img_width;
  auto pixel_size = image_width / fcols;

  // from top to bottom
  auto v_dir = -1.0 * this->m_camera.m_orientation->ydir;
  // from left to right
  auto h_dir = this->m_camera.m_orientation->xdir;

  // steps in rows and cols, should be same size as pixels are always quadratic
  this->m_col_step = h_dir * pixel_size;
  this->m_row_step = v_dir * pixel_size;

  // auto image_height = pixel_size * frows;
  // this->m_top_left = this->m_camera.m_orientation->pos
  //     + (h_dir * pixel_size * 0.5) // NOLINT(*-magic-numbers)
  //     - (0.5 * pixel_size * fcols * h_dir)   // NOLINT(*-magic-numbers)
  //     - (0.5 * pixel_size * frows * v_dir)  // NOLINT(*-magic-numbers)
  //     + (v_dir * pixel_size * 0.5); // NOLINT(*-magic-numbers)

  this->m_top_left = this->m_camera.m_orientation->pos +
      ( ( 1 - fcols ) * 0.5 * pixel_size ) * h_dir    // NOLINT(*-magic-numbers)
      + ( ( 1 - frows ) * 0.5 * pixel_size ) * v_dir; // NOLINT(*-magic-numbers)

  this->reset_ray( start );
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::iterator::operator*() -> Ray<Color, BaseVec> & {
  return this->m_ray;
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::iterator::operator->() -> Ray<Color, BaseVec> * {
  return &( this->m_ray );
}

template<class Color, class BaseVec>
inline void Camera<Color, BaseVec>::iterator::reset_ray() noexcept {

  auto new_ray_base = this->m_top_left // from top left
                                       // add row steps
      + ( this->m_row_step * static_cast<typename BaseVec::elem_type>( this->m_current_row ) )
      // add col steps
      + ( this->m_col_step * static_cast<typename BaseVec::elem_type>( this->m_current_col ) );

  this->m_ray.origin = new_ray_base;

  this->m_ray.move_to( new_ray_base, this->m_camera.focus(), new_ray_base );
}

template<class Color, class BaseVec>
inline void Camera<Color, BaseVec>::iterator::reset_ray( size_t index ) noexcept {
  std::tie( this->m_current_row, this->m_current_col ) =
      this->m_camera.m_image.unravel_index( index );
  this->reset_ray();
}

template<class Color, class BaseVec>
inline void Camera<Color, BaseVec>::iterator::reset_ray( size_t row, size_t col ) noexcept {
  this->m_current_row = row;
  this->m_current_col = col;
  this->reset_ray();
}

template<class Color, class BaseVec>
inline void Camera<Color, BaseVec>::iterator::increment_ray() noexcept {

  if ( ++( this->m_current_col ) < this->m_camera.m_image.cols ) {
    this->m_ray.origin += this->m_col_step;
  } else {
    // increment row and return to first col
    this->m_current_row += 1;
    this->m_current_col = 0;
    this->m_ray.origin =
        ( static_cast<typename BaseVec::elem_type>( this->m_current_row ) * this->m_row_step ) +
        this->m_top_left;
  }

  this->m_ray.move_to( this->m_ray.origin, this->m_camera.focus(), this->m_ray.origin );
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::iterator::operator++() -> typename Camera<Color, BaseVec>::iterator & {
  increment_ray();
  return *this;
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::iterator::operator++( int ) ->
    typename Camera<Color, BaseVec>::iterator {
  auto prev = iterator( this->m_camera );
  prev.reset_ray( this->m_current_row, this->m_current_col );
  increment_ray();
  return prev;
}

template<class Color, class BaseVec>
void Camera<Color, BaseVec>::iterator::operator+=( size_t const &offset ) {
  auto current_index =
      this->m_camera.m_image.ravel_index( this->m_current_row, this->m_current_row );
  this->reset_ray( current_index + offset );
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::iterator::operator==( iterator const &other ) const -> bool {
  return this->m_current_row == other.m_current_row && this->m_current_col == other.m_current_col;
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::iterator::operator!=( iterator const &other ) const -> bool {
  return this->m_current_row != other.m_current_row || this->m_current_col != other.m_current_col;
}

template<class Color, class BaseVec>
void Camera<Color, BaseVec>::iterator::apply_current_ray() const {
  this->m_camera.apply_ray( this->m_current_row, this->m_current_col, this->m_ray );
}

template struct Camera<ColorVec, PosVec>::iterator;
} // namespace core
