#include "core/linalg.hpp"

#include <array>
#include <cmath>
#include <cstring>
#include <type_traits>

template<typename T, size_t n>
constexpr auto init_arr( T val ) -> std::array<T, n> {
  auto ret = std::array<T, n>();
  ret.fill( val );
  return ret;
}

namespace core {
template<typename T, size_t n>
ArrayN<T, n>::ArrayN( T const &_val ) : m_values( init_arr<T, n>( _val ) ){};

// template<typename T, size_t n>
// ArrayN<T, n>::ArrayN( array<T, n> values ) : m_values( values ){};

template<typename T, size_t n>
ArrayN<T, n>::ArrayN( array<T, n> const &values ) : m_values( values ){};

template<typename T, size_t n>
auto ArrayN<T, n>::operator[]( rt_index index ) const -> T {
  return this->m_values[ index ];
};

template<typename T, size_t n>
auto ArrayN<T, n>::operator[]( rt_index index ) -> T & {
  return this->m_values[ index ];
};

template<typename T, size_t n>
void // NOLINT Updating Values from const Arr
ArrayN<T, n>::operator=( ArrayN<T, n>::array_type const &other ) {
  for ( auto it = other.begin(); it != other.end(); ++it ) {
    this->m_values[ it - other.begin() ] = *it;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator-=( ArrayN<T, n> const &other ) {
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->m_values[ it - other.cbegin() ] = this->m_values[ it - other.cbegin() ] - *it;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator-=( T const &scalar ) {
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it -= scalar;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator+=( ArrayN<T, n> const &other ) {
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->m_values[ it - other.cbegin() ] = this->m_values[ it - other.cbegin() ] + *it;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator+=( T const &scalar ) {
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it += scalar;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator*=( ArrayN<T, n> const &other ) {
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->m_values[ it - other.cbegin() ] = this->m_values[ it - other.cbegin() ] * *it;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator*=( T const &scalar ) {
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it *= scalar;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator/=( ArrayN<T, n> const &other ) {
  for ( auto it = other.cbegin(); it != other.cend(); ++it ) {
    this->m_values[ it - other.cbegin() ] = this->m_values[ it - other.cbegin() ] / *it;
  }
}

template<typename T, size_t n>
void ArrayN<T, n>::operator/=( T const &scalar ) {
  for ( auto it = this->begin(); it != this->end(); ++it ) {
    *it /= scalar;
  }
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator-( ArrayN<T, n> const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret -= other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator-( T const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret -= other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator+( ArrayN<T, n> const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret += other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator+( T const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret += other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator*( ArrayN<T, n> const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret *= other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator*( T const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret *= other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator/( ArrayN<T, n> const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret /= other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator/( T const &other ) const -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( *this );
  ret /= other;
  return ret;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator==( ArrayN<T, n> const &other ) const -> bool {

  for ( size_t i = 0; i < n; ++i ) {
    if ( this->m_values[ i ] != other[ i ] ) { // NOLINT no bound cehcking needed, as we use n
      return false;
    }
  }

  return true;
}

template<typename T, size_t n>
auto ArrayN<T, n>::operator!=( ArrayN<T, n> const &other ) const -> bool {

  return !this->operator==( other );
}

template<typename T, size_t n>
ArrayN<T, n>::operator array<T, n>() const {
  return this->m_values;
}

template<typename T, size_t n>
auto ArrayN<T, n>::cget() const -> std::array<T, n> const & {
  return this->m_values;
}

template<typename T, size_t n>
auto ArrayN<T, n>::get() -> std::array<T, n> & {
  return this->m_values;
}

template<typename T, size_t n>
auto ArrayN<T, n>::begin() -> typename ArrayN<T, n>::iterator {
  return m_values.begin();
}

template<typename T, size_t n>
auto ArrayN<T, n>::end() -> typename ArrayN<T, n>::iterator {
  return m_values.end();
}

template<typename T, size_t n>
auto ArrayN<T, n>::cbegin() const -> typename ArrayN<T, n>::const_iterator {
  return m_values.cbegin();
}

template<typename T, size_t n>
auto ArrayN<T, n>::cend() const -> typename ArrayN<T, n>::const_iterator {
  return m_values.cend();
}

template<typename T, size_t n>
void ArrayN<T, n>::normalize() {
  T norm = this->norm();
  for ( size_t i = 0; i < ArrayN<T, n>::dim; ++i ) {
    this->m_values[ i ] = this->m_values[ i ] / norm;
  }
}

template<typename T, size_t n>
auto ArrayN<T, n>::norm() const -> T {
  // square
  T sqsum = 0;
  for ( auto it = this->cbegin(); it != this->cend(); ++it ) {
    sqsum += ( *it ) * ( *it );
  }
  return std::sqrt( sqsum );
}

template<typename T, size_t n>
auto ArrayN<T, n>::sum() const -> T {
  T sum = 0;
  for ( auto it = this->cbegin(); it != this->cend(); ++it ) {
    sum += *it;
  }
  return sum;
}

template<typename T, size_t n>
auto ArrayN<T, n>::dot( ArrayN<T, n> const &other ) const -> T {
  T sum = 0;
  for ( size_t i = 0; i < n; ++i ) {
    sum += ( *this )[ i ] * other[ i ];
  }
  return sum;
}

template<typename T, size_t n>
auto ArrayN<T, n>::cos( ArrayN<T, n> const &other ) const -> T {
  T this_i;
  T other_i;
  T sqsum_this = 0;
  T sqsum_other = 0;
  T _dot = 0;

  for ( size_t i = 0; i < n; ++i ) {
    this_i = ( *this )[ i ];
    other_i = other[ i ];

    _dot += this_i * other_i;
    sqsum_this += this_i * this_i;
    sqsum_other += other_i * other_i;
  }

  return _dot / ( std::sqrt( sqsum_other ) * std::sqrt( sqsum_this ) );
}

template<class BaseVec>
CoordBase<BaseVec>::CoordBase( BaseVec const &position )
    : ydir( { 1, 0, 0 } ), xdir( { 0, 1, 0 } ), zdir( { 0, 0, 1 } ), pos( position ){};

template<class BaseVec>
CoordBase<BaseVec>::CoordBase(
    BaseVec const &upward, BaseVec const &forward, BaseVec const &position )
    : ydir( upward ), xdir( cross( forward, upward ) ), zdir( forward ), pos( position ) {
  this->normalize();
};

template<class BaseVec>
void CoordBase<BaseVec>::normalize() {
  this->ydir.normalize();
  this->xdir.normalize();
  this->zdir.normalize();
}

template<class Arr>
auto operator<<( std::ostream &stream, Arr const &arr ) -> std::ostream & {

  constexpr char const *pre = "Arr(";

  stream.write( pre, strlen( pre ) ); // NOLINT pre will always be small

  size_t i = 0; // NOLINT Loop var
  for ( ; i < ( arr.dim - 1 ); ++i ) {
    stream << arr[ i ];
    stream.write( ", ", 2 );
  }
  stream << arr[ arr.dim - 1 ];
  stream.write( ")", 1 );

  return stream;
}

auto operator<<( std::ostream &stream, FArray3 const &arr ) -> std::ostream & {
  return operator<< <FArray3>( stream, arr );
};
auto operator<<( std::ostream &stream, FArray4 const &arr ) -> std::ostream & {
  return operator<< <FArray4>( stream, arr );
};
auto operator<<( std::ostream &stream, DArray3 const &arr ) -> std::ostream & {
  return operator<< <DArray3>( stream, arr );
};
auto operator<<( std::ostream &stream, DArray4 const &arr ) -> std::ostream & {
  return operator<< <DArray4>( stream, arr );
};

template<typename T, size_t n>
auto operator-( T const &scalar, ArrayN<T, n> const &arr ) -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( arr );
  for ( size_t i = 0; i < ArrayN<T, n>::dim; ++i ) {
    ret[ i ] = scalar - ret[ i ];
  }
  return ret;
}
template<typename T, size_t n>
auto operator+( T const &scalar, ArrayN<T, n> const &arr ) -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( arr );
  for ( size_t i = 0; i < ArrayN<T, n>::dim; ++i ) {
    ret[ i ] = scalar + ret[ i ];
  }
  return ret;
}
template<typename T, size_t n>
auto operator*( T const &scalar, ArrayN<T, n> const &arr ) -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( arr );
  for ( size_t i = 0; i < ArrayN<T, n>::dim; ++i ) {
    ret[ i ] = scalar * ret[ i ];
  }
  return ret;
}
template<typename T, size_t n>
auto operator/( T const &scalar, ArrayN<T, n> const &arr ) -> ArrayN<T, n> {
  auto ret = ArrayN<T, n>( arr );
  for ( size_t i = 0; i < ArrayN<T, n>::dim; ++i ) {
    ret[ i ] = scalar / ret[ i ];
  }
  return ret;
}

template<typename T>
auto cross( ArrayN<T, 3> const &some, ArrayN<T, 3> const &other ) -> ArrayN<T, 3> {
  auto u = some;  // NOLINT for brevity reasons
  auto v = other; // NOLINT for brevity reasons
  return ArrayN<T, 3>(
      { u[ 1 ] * v[ 2 ] - u[ 2 ] * v[ 1 ], u[ 2 ] * v[ 0 ] - u[ 0 ] * v[ 2 ],
        u[ 0 ] * v[ 1 ] - u[ 1 ] * v[ 0 ] } );
}

template<typename T, ColorFormat mode>
ColorVector<T, mode>::ColorVector( ArrayN<T, static_cast<size_t>( mode )> const &arr )
    : ArrayN<T, static_cast<size_t>( mode )>( arr ) {}

// 3D Space
template class ArrayN<rt_float, 3>;
template class ArrayN<rt_float, 4>;
template class CoordBase<ArrayN<rt_float, 3>>;
template auto cross<rt_float>( ArrayN<rt_float, 3> const &, ArrayN<rt_float, 3> const & )
    -> ArrayN<rt_float, 3>;

template auto operator+( float const &, FArray3 const & ) -> FArray3;
template auto operator-( float const &, FArray3 const & ) -> FArray3;
template auto operator*( float const &, FArray3 const & ) -> FArray3;
template auto operator/( float const &, FArray3 const & ) -> FArray3;

template auto operator+( double const &, DArray3 const & ) -> DArray3;
template auto operator-( double const &, DArray3 const & ) -> DArray3;
template auto operator*( double const &, DArray3 const & ) -> DArray3;
template auto operator/( double const &, DArray3 const & ) -> DArray3;

// template std::ostream& operator<< <ArrayN<rt_float, 3>> (std::ostream&, const ArrayN<rt_float, 3>
// &);

// 4D Space
// template class ArrayN<rt_float, 4>;
// template class SpatialVector<rt_float, 4>;
// template class CoordBase<SpatialVector<rt_float, 4>>;

// Color Spaces
template class ColorVector<rt_float, ColorFormat::RGB>;
// template class ColorVector<rt_float, ColorFormat::RGBA>;
} // namespace core
