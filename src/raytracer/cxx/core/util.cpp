#include "core/util.hpp"

#include "core/linalg.hpp"

// template<typename T>
// divmod<T>::divmod( core::rt_size number, core::rt_size quot )
//     : div( static_cast<T>( static_cast<core::rt_size>( number / quot ) ) ),
//       rem( static_cast<T>( static_cast<core::rt_size>( number % quot ) ) ) {}

template<>
divmod<core::rt_float>::divmod( core::rt_size number, core::rt_size quot )
    : div( static_cast<core::rt_float>(
          static_cast<core::rt_size>( number / quot ) ) ), // ensure div is in integer contexte
      rem( static_cast<core::rt_float>( ( number % quot ) ) ) {}

template<>
divmod<core::rt_size>::divmod( core::rt_size number, core::rt_size quot )
    : div( number / quot ), rem( number % quot ) {}

template struct divmod<core::rt_float>;
template struct divmod<core::rt_size>;
