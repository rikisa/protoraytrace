#include "core/camera.hpp"

#include "core/typedefs.hpp"

#include <algorithm>
#include <memory>

namespace core {

template<class Color, class BaseVec>
Ray<Color, BaseVec>::Ray() : origin( 0 ), base( 0 ), dir( 0 ) {}

template<class Color, class BaseVec>
Ray<Color, BaseVec>::Ray( BaseVec const &origin, BaseVec const &point_a, BaseVec const &point_b ) {
  this->origin = origin;
  this->move_to( origin, point_a, point_b );
}

template<class Color, class BaseVec>
void Ray<Color, BaseVec>::move_to(
    BaseVec const &new_base, BaseVec const &dir_from, BaseVec const &dir_to ) {
  this->base = new_base;

  for ( size_t i = 0; i < BaseVec::dim; ++i ) {
    this->dir[ i ] = dir_to[ i ] - dir_from[ i ];
  }

  this->dir.normalize();
}

template<class Color, class BaseVec>
void Ray<Color, BaseVec>::move_to( BaseVec const &new_base ) {
  this->base = new_base;
}

template<class Color, class BaseVec>
void Ray<Color, BaseVec>::sample( ColorVec const &color ) {
  this->m_samples += 1;
  this->m_color += color;
}

template<class Color, class BaseVec>
auto Ray<Color, BaseVec>::color() const -> Color {
  return Color( this->m_color / std::max( this->m_samples, _m_min_sample ) );
}

template<class Color, class BaseVec>
Camera<Color, BaseVec>::Camera(
    Image<Color> &image, typename BaseVec::elem_type image_width,
    CoordBase<BaseVec> const &orientation, typename BaseVec::elem_type focal_len )
    : m_image( image ), m_orientation( std::make_unique<CoordBase<BaseVec>>( orientation ) ),
      m_focal_len( focal_len ), m_img_width( image_width ) {

  this->update_focal_point();
}

template<class Color, class BaseVec>
void Camera<Color, BaseVec>::update_focal_point() {
  this->m_focus = this->m_orientation->pos - ( this->m_orientation->zdir * this->m_focal_len );
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::orientation() const -> CoordBase<BaseVec> const * {
  return this->m_orientation.get();
}

template<class Color, class BaseVec>
void Camera<Color, BaseVec>::look_at( BaseVec poi ) {
  auto new_fwd = poi - this->m_orientation->pos;
  auto new_up = cross( new_fwd, this->m_orientation->xdir );
  // auto new_base = std::make_unique<CoordBase<BaseVec>>( new_up, new_fwd,
  // this->m_orientation->pos ); this->m_orientation.reset( new_base.release() );
  this->m_orientation =
      std::make_unique<CoordBase<BaseVec>>( new_up, new_fwd, this->m_orientation->pos );
  this->update_focal_point();
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::focus() -> BaseVec const & {
  return this->m_focus;
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::image() -> Image<Color> const & {
  return this->m_image;
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::begin() -> typename Camera<Color, BaseVec>::iterator {
  return Camera<Color, BaseVec>::iterator( *this );
}

template<class Color, class BaseVec>
auto Camera<Color, BaseVec>::end() -> typename Camera<Color, BaseVec>::iterator {
  return Camera<Color, BaseVec>::iterator( *this, this->m_image.size );
}

template<class Color, class BaseVec>
void Camera<Color, BaseVec>::apply_ray(
    rt_index row, rt_index col, Ray<Color, BaseVec> const &ray ) {
  this->m_image.set_pixel( row, col, ray.color() );
}

template class Camera<ColorVec, PosVec>;
template struct Ray<ColorVec, PosVec>;
} // namespace core
