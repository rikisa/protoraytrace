// Glue code between command line interface and lua controls
// Supplies factory functions for the various components
// and extends them

#include "components.hpp"

#include "glview.hpp"
#include "luarepl.hpp"
#include "luascript.hpp"

#include <fstream>
#include <sstream>

namespace components {

namespace {
// nonconst bc it is supposed to be a single compilation unit with main app
std::shared_ptr<engine::Controller> controller = nullptr;
std::shared_ptr<engine::Viewer> viewer = nullptr;
std::shared_ptr<event::Handler> eventhandler = nullptr;

auto res_rpath = std::filesystem::path( ".." );
auto lua_setup_rpath = res_rpath / "lua" / "setup.lua";
auto vert_rpath = res_rpath / "glsl" / "canvas.vert";
auto frag_rpath = res_rpath / "glsl" / "canvas.frag";

auto logger = spdlog::stdout_color_mt( "main" );

auto _get_controller( opt::ParseResult const &parameter, std::filesystem::path lua_setup_path )
    -> std::shared_ptr<engine::Controller> {

  if ( parameter.count( "lua-script" ) > 0 ) {
    if ( parameter.count( "lua-script" ) != 1 ) {
      throw std::invalid_argument( "Only a single script file" );
    };
    auto path = parameter[ "lua-script" ].as<std::string>();
    return std::make_shared<luacontrol::Script>( path );
  };

  if ( parameter.count( "lua-interactive" ) > 0 ) {
    // return LuaREPL( std::move(sol_lua) );
    auto lua_repl = std::make_shared<luacontrol::REPL>();
    // generate some stuff already there
    if ( std::filesystem::exists( lua_setup_path ) ) {
      lua_repl->run_script( lua_setup_path );
      logger->info( "Loaded {:s}", lua_setup_path.c_str() );
      return lua_repl;
    }
    logger->warn( "Could not find lua setup file {:s}", lua_setup_path.c_str() );
    return lua_repl;
  };

  throw std::invalid_argument( "Need either 'lua-script' or 'lua-interactive'" );
}

auto _get_viewer( std::filesystem::path vert_file_path, std::filesystem::path frag_file_path )
    -> std::shared_ptr<engine::Viewer> {

  auto glview = std::make_shared<glview::GLViewer>();

  auto vert_src = std::ifstream( vert_file_path );
  auto frag_src = std::ifstream( frag_file_path );

  logger->debug( "Loading shaders:\n\t{}\n\t{}", vert_file_path.c_str(), frag_file_path.c_str() );
  if ( vert_src and frag_src ) {
    glview->set_sources(
        std::string( std::istreambuf_iterator<char>( vert_src ), std::istreambuf_iterator<char>() ),
        std::string(
            std::istreambuf_iterator<char>( frag_src ), std::istreambuf_iterator<char>() ) );
  } else {
    logger->warn(
        "Could not find all shader files: Found vert={}, found frag={}", bool( vert_src ),
        bool( frag_src ) );
  };

  return glview;
}

} // namespace

// adds needed infromation to Comandline Interface
void append_options( opt::Options &opts ) {

  opts.add_options()(
      "s,lua-script", "Path to luascript to execute",
      cxxopts::value<std::string>() )( "i,lua-interactive", "Path to luascript to execute" );
};

auto setup( opt::ParseResult const &parameter, std::filesystem::path exe_dir ) -> int {

  auto vert_file = exe_dir / vert_rpath;
  auto frag_file = exe_dir / frag_rpath;
  auto lua_setup_path = exe_dir / lua_setup_rpath;
  logger->debug( "Looking for Lua setup file '{:s}'", lua_setup_path.c_str() );
  // Use factory functions in each module, so that the knowledge which kind
  // of paramters are needed is only in the module itself.
  try {
    controller = components::_get_controller( parameter, lua_setup_path );
    viewer = components::_get_viewer( parameter, vert_file, frag_file );
  } catch ( std::invalid_argument &e ) {
    logger->error( "{}", e.what() );
    return EXIT_FAILURE;
  }

  eventhandler = std::make_shared<event::Handler>();

  event::connect( controller, eventhandler );
  event::connect( viewer, eventhandler );

  return EXIT_SUCCESS;
};

auto get_controller() -> std::shared_ptr<engine::Controller> { return controller; }

auto get_viewer() -> std::shared_ptr<engine::Viewer> { return viewer; }

auto get_eventhandler() -> std::shared_ptr<event::Handler> { return eventhandler; }

auto get_console_logger() -> std::shared_ptr<spdlog::logger> { return logger; };

} // namespace components
