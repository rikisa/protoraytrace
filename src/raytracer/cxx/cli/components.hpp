#pragma once

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#include "cxxopts.hpp"
#include "engine/control.hpp"
#include "engine/view.hpp"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <filesystem>
#include <memory>

namespace opt = cxxopts;

namespace components {

void append_options( opt::Options &opts );
/*
 * Setup components, might exit on error
 */
auto setup( opt::ParseResult const & /*parameter*/, std::filesystem::path /*exe_dir*/ ) -> int;

auto get_controller() -> std::shared_ptr<engine::Controller>;
auto get_viewer() -> std::shared_ptr<engine::Viewer>;
auto get_eventhandler() -> std::shared_ptr<event::Handler>;

auto get_console_logger() -> std::shared_ptr<spdlog::logger>;
auto get_file_logger() -> std::shared_ptr<spdlog::logger>;

void cleanup();

} // namespace components
