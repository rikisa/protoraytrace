/* CLI version of the raytracer
 */

#include "components.hpp"
#include "version.hpp"

#include <cerrno>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <vector>

#if defined FOR_LINUX
#define PLATFORMSTR "(linux)"
#elif defined FOR_WIN
#define PLATFORMSTR "(win)"
#else
#define PLATFORMSTR "(unknow)"
#endif

using std::cout;
using ArgVector = std::vector<std::string>;
namespace fs = std::filesystem;

constexpr auto short_description = "A funky little raytracer " PLATFORMSTR;

/*
 * Parse paramete, throw if something is off, handle basic options
 * might stop program exectution
 */
auto parse_cmdline( opt::Options cmdlopts, int argc, char **argv ) -> opt::ParseResult {
  // show help if no parameters are provided
  opt::ParseResult parameter;

  auto logger = components::get_console_logger();

  try {
    parameter = cmdlopts.parse( argc, argv );
  } catch ( opt::option_not_exists_exception &e ) {
    logger->error( "{}", e.what() );
    cout << cmdlopts.help();
    std::exit( EXIT_FAILURE ); // NOLINT(*-mt-unsafe)
  }

  // default loglevel
  spdlog::set_level( spdlog::level::info );
  if ( parameter.count( "verbose" ) > 0 ) {
    spdlog::set_level( spdlog::level::debug );
  }

  // version and help
  if ( parameter.count( "version" ) > 0 ) {
    cout << version::app.full << "\n";
    std::exit( EXIT_SUCCESS ); // NOLINT(*-mt-unsafe)
  }
  if ( parameter.count( "help" ) > 0 ) {
    cout << cmdlopts.help();
    std::exit( EXIT_SUCCESS ); // NOLINT(*-mt-unsafe)
  }

  return parameter;
};

auto main( int argc, char **argv ) -> int {

  auto const *bin_name = argv[ 0 ]; // NOLINT(*-pointer-arithmetic)
  auto const bin_dir = fs::absolute( fs::path( bin_name ) ).parent_path();

  spdlog::set_pattern( "%H:%M:%S.%e %8!n %t %^%L%$ %v" );

  auto logger = components::get_console_logger();
  // // comandlineparser with info
  opt::Options cmdlopts(
      bin_name,
      std::string( short_description ) + "\n'" + version::app.name + "' version " +
          version::app.full + "\n    Lua: " + version::lua + "\n" );

  // general options
  cmdlopts.add_options()( "v,verbose", "Verbose logging" )( "version", "Print version and quit" )(
      "h,help", "Print help and quit" );

  // append CLI options for the available components
  components::append_options( cmdlopts );

  // parse commandline args and return parameter, exit on error
  auto parameter = parse_cmdline( cmdlopts, argc, argv );

  // setup the components based on paramter
  // exit on error
  auto ret = components::setup( parameter, bin_dir );
  if ( ret != 0 ) {
    cout << cmdlopts.help();
    std::exit( EINVAL ); // NOLINT(*-mt-unsafe)
  }

  auto controller = components::get_controller();
  auto viewer = components::get_viewer();
  auto eventhandler = components::get_eventhandler();

  // Camera cam;
  // viewer->observe( cam.image.get_view() );

  eventhandler->emit( event::Basic::START );

  // wait for components to stop
  controller->wait();
  controller = nullptr;
  logger->debug( "Controller stopped" );
  viewer->wait();
  viewer = nullptr;
  logger->debug( "Viewer stopped" );

  logger->info( "Done" );
  return EXIT_SUCCESS;
}
