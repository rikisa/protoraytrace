#include "core/camera.hpp"
#include "core/linalg.hpp"
#include "core/objects.hpp"

template<class Color, class BaseVec>
void trace( core::Camera<Color, BaseVec> & /*cam*/, Scene<Color, BaseVec> const & /*unused*/ );
