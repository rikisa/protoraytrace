/*
 * Everything binding engine functions to sol lua
 */

#pragma once

#include "core/image.hpp"
#include "core/linalg.hpp"
#include "sol/sol.hpp" // NOLINT

/*
 * Create function bindings to the bits and pieces of the engine
 */
auto bind_engine_functions( sol::state /*lua*/ ) -> sol::state;

template<class Color>
auto get_active_image() -> core::ImageView<Color> &;
