#pragma once

#include "events.hpp"

#include <memory>
#include <thread>

/** \brief Abstract baseclass for raytracer control
 *
 *  The RTControl is responsible vor taking / processing user input
 *  or input files. Builds the Scene and is started and stopped by the main
 *  thread. Must be movable.
 */
namespace engine {

class Controller : public event::Observer {

public:
  virtual ~Controller() = default;
  // start a new thread / call worker function
  void start() override = 0;
  // Signales the controller to stop
  void stop() override = 0;
};

} // namespace engine
