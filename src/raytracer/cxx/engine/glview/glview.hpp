#pragma once

#include "engine/view.hpp"

#include <memory>

namespace glview {

class GLViewer : public engine::Viewer {

public:
  GLViewer();
  // start ant take over thread of execution
  void start() override;
  void stop() override;
  // void observe( ImageView<ColorVec> image ) override;
  void set_sources( std::string /*vert_source*/, std::string /*frag_source*/ );
  void wait();

private:
  bool m_should_stop = false;
  std::string vert_src{};
  std::string frag_src{};
};

} // namespace glview
