/* Wrapps raw readline and lua code to do the interactive repl
 *
 */
#include "luarepl.hpp"

#include "events.hpp"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <string>

// cstuff for luafuncs
#include <csignal>
#include <cstdlib>

extern "C" {
#include "luafuncs.h"
}

namespace luacontrol {

// hidden for outside world
namespace {

// logger for this module
auto logger = spdlog::stdout_color_mt( "luarepl" );

void _log_info( std::string msg ) { logger->info( msg ); }

} // namespace

REPL::REPL( sol::state lua_sol ) : LuaController( std::move( lua_sol ) ){};
REPL::REPL() { this->name = "LuaControl"; };

REPL::virtual ~REPL() {
  this->m_stop_repl = true;
  // does not work during deconstruction
  // auto logger = spdlog::default_logger();
  // logger->debug("Waiting for thread to stop");
  this->m_repl_thread.join();
}

void REPL::enter_repl() {
  int status = 0;
  this->m_stopped = false;

  auto *L = this->get_luastate().lua_state();

  logger->info( "Entering Lua read/eval loop" );
  // auto rl_readline_name = "lua";
  while ( ( status = loadline( L ) ) != -1 ) {
    if ( status == LUA_OK ) {
      status = docall( L, 0, LUA_MULTRET );
    }
    if ( status == LUA_OK ) {
      l_print( L );
    } else {
      report( L, status );
    }

    // must happen here to step out of loop
    if ( this->m_stop_repl ) {
      break;
    }
  }

  lua_settop( L, 0 ); /* clear stack */
  lua_writeline();

  logger->info( "Left Lua read/eval loop" );

  // controll loop reached its end, emit stop signal
  // if stopped because of EOF controll sequence
  if ( !this->m_stop_repl ) {
    this->quit();
  }
  this->m_stopped = true;
}

void REPL::start() {
  logger->debug( "Initialize Lua" );

  auto lua = this->get_luastate();

  lua.set_function( "quit", &REPL::quit, this );
  lua.set_function( "log", &_log_info );

  this->m_stop_repl = false;
  // this->enter_repl();

  this->m_repl_thread = std::thread( &REPL::enter_repl, this );
  logger->debug( "Started Lua in new thread" );
};

/*
 * Quitting by sending stop signal
 */
void REPL::quit() { this->emit( event::Basic::STOP ); }

void REPL::stop() {
  if ( !this->m_stopped ) {
    logger->debug( "Recieved Stop" );
    this->m_stop_repl = true;
  }
}

} // namespace luacontrol
