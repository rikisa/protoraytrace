#include "engine/control.hpp"
#include "luarepl.hpp"
#include "sol/sol.hpp"

#include <memory>

int main() {
  sol::state lua;
  luacontrol::REPL repl;
  repl.start();
  return 0;
};
