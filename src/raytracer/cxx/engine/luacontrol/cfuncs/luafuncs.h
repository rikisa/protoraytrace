/* Copyright (C) 1994-2022 Lua.org, PUC-Rio.
 * For licence information, please see LICENCE in this folder
 */

#pragma once

// lua headers
#include "lua.h"

auto docall( lua_State * /*L*/, int /*narg*/, int /*nres*/ ) -> int;
auto loadline( lua_State * /*L*/ ) -> int;

auto report( lua_State * /*L*/, int /*status*/ ) -> int;
void l_print( lua_State * /*L*/ );

auto msghandler( lua_State * /*L*/ ) -> int;
auto incomplete( lua_State * /*L*/, int /*status*/ ) -> int;

/*
 * Lua either set its internal signalhandler
 * or returns on the default. These two case
 * are reflected in set_luasignal and unset_luasignal (back to default)
 */
void set_luasighandler( lua_State * /*L*/, int /*sig*/ );
void unset_luasighandler( int /*sig*/ );
