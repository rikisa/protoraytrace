/* test embeding of lua interpreter into c++ using sol
 */

#pragma once

#include "events.hpp"
#include "luacontrol.hpp"

#include <memory>
#include <thread>

namespace luacontrol {

class REPL : public LuaController {

public:
  void start() override;
  void stop() override;

  REPL();
  virtual ~REPL();
  explicit REPL( sol::state /*lua_sol*/ );
  REPL( const REPL & ) = delete;

private:
  // Lua REPL
  void enter_repl();
  // quit execution of REPL
  void quit();
  bool m_stop_repl{};
  std::thread m_repl_thread;
};

} // namespace luacontrol
