#version 410 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;
uniform vec2 param;
vec3 offset;
out vec3 ourColor;
out vec2 TexCoord;

void main()
{
   // funny stuff
   // offset = vec3(1 + sin(param[1] * 1e-2), 1, 1);
   // gl_Position = vec4(aPos * offset , param[0]);
   gl_Position = vec4(aPos , param[0]);
   ourColor = vec3(aPos);
   // ourColor = aColor;
   TexCoord = aTexCoord;
}
