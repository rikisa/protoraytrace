if( EXISTS "${CMAKE_BINARY_DIR}/compile_commands.json" )
    file( CREATE_LINK ${CMAKE_BINARY_DIR}/compile_commands.json ${CMAKE_SOURCE_DIR}/compile_commands.json
          COPY_ON_ERROR SYMBOLIC )
endif()

# # export include dirs for for vims Ale and write local vimrc
# # get_property(IncludeDirs DIRECTORY src PROPERTY INCLUDE_DIRECTORIES)
# foreach(idir ${ALL_INCLUDES})
#     set(AleCppIncludeOptions "${AleCppIncludeOptions} -I${idir}")
#     # message(STATUS "Exporting '${idir}' to vimplugin Ale")
# endforeach()
# set(AleCppIncludeOptions "let g:ale_cpp_clang_options=\"-std=gnu++17${AleCppIncludeOptions}\"")
# file(WRITE .vimrc ${AleCppIncludeOptions})
