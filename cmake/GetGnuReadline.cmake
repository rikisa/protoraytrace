ExternalProject_Add(
    readline_repository
    GIT_REPOSITORY "http://git.savannah.gnu.org/git/readline.git"
    GIT_TAG "readline-8.1"
    GIT_SHALLOW ON
    GIT_PROGRESS ON # show progress of download
    PREFIX "_deps/readline_repository"
    CONFIGURE_COMMAND <SOURCE_DIR>/configure
    BUILD_COMMAND ${CMAKE_COMMAND} -E env
        CC=${CMAKE_C_COMPILER}
        make static
    INSTALL_COMMAND ""
    UPDATE_COMMAND ""
)

ExternalProject_Get_property( readline_repository BINARY_DIR )
ExternalProject_Get_property( readline_repository SOURCE_DIR )

# add imported libraries
add_library( readline STATIC IMPORTED )
add_library( history STATIC IMPORTED )

# sources must b e downloaded
add_dependencies( history readline_repository )
add_dependencies( readline readline_repository )

# set linking directories
set_property( TARGET readline PROPERTY IMPORTED_LOCATION ${BINARY_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}readline${CMAKE_STATIC_LIBRARY_SUFFIX} )
set_property( TARGET history PROPERTY IMPORTED_LOCATION ${BINARY_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}history${CMAKE_STATIC_LIBRARY_SUFFIX} )

# add headers/interfaces for dependees
target_include_directories( readline INTERFACE ${SOURCE_DIR} )
target_include_directories( history INTERFACE ${SOURCE_DIR} )
