#include "common/utils.hpp"
#include "core/camera.hpp"
#include "core/linalg.hpp"
#include "test_main.hpp"

using core::ColorVec;
using core::DArray3;
using core::PosVec;
using core::Ray;

constexpr double small_nonzero = 0.1;

// NOLINTBEGIN(*-cognitive-complexity, *-magic-numbers)
TEST_CASE( "Ray defaults", "[Ray][core][rendering]" ) {

  Ray<ColorVec, PosVec> ray_default;
  DArray3 zero( { 0, 0, 0 } );

  INFO( "ray.origin(=" << ray_default.origin << ") == zero = " << ( ray_default.origin == zero ) );
  REQUIRE( ( ray_default.origin == zero ) );
  INFO( "ray.base(=" << ray_default.base << ") == zero = " << ( ray_default.base == zero ) );
  REQUIRE( ( ray_default.base == zero ) );
  REQUIRE( ( ray_default.dir == zero ) );

  INFO( "ray.color: " << ray_default.color() );
  REQUIRE( ( ray_default.color() == zero ) );

  DArray3 vec_ori( { 0, 0, 1 } );
  DArray3 vec_a( { 0, 0, 0 } );
  DArray3 vec_b( { -0, 0, small_nonzero } );
  DArray3 ought_dir( { -0, 0, 1 } );

  Ray<ColorVec, PosVec> ray_non_default( vec_ori, vec_a, vec_b );

  INFO(
      "ndray.origin " << ray_non_default.origin << " ndray.base " << ray_non_default.base
                      << " ndray.dir " << ray_non_default.dir << "\n" )
  REQUIRE( ( ray_non_default.origin == vec_ori ) );
  REQUIRE( ( ray_non_default.base == vec_ori ) );
  REQUIRE( ( ray_non_default.dir == ought_dir ) );
}

TEST_CASE( "Ray move_to", "[Ray][core][rendering]" ) {

  DArray3 ori( { 0, 0, 0 } );
  DArray3 new_base( { 0, 0, 1 } );
  DArray3 vec_a( { 1, 1, 1 } );
  DArray3 vec_b( { 1, 1 + small_nonzero, 1 } );
  DArray3 dir_ab( { 0.0, 1.0, 0.0 } );

  Ray<ColorVec, PosVec> ray;
  Ray<ColorVec, PosVec> ray2( vec_a, vec_b, dir_ab ); // some bogus stuff
  REQUIRE( ray2.dir.norm() == 1 );

  SECTION( "Origin is invariant to move" ) {
    ray.move_to( new_base, vec_a, vec_b );
    REQUIRE( ray.origin == ori );
    REQUIRE( ray.base == new_base );
    REQUIRE( ray.dir == dir_ab );
  }

  SECTION( "Directional sign" ) {
    ray.move_to( new_base, vec_b, vec_a );
    REQUIRE( ray.dir == -1.0 * dir_ab );
  }

  SECTION( "Keep dir when moving base only" ) {
    auto old_dir = ray2.dir;
    REQUIRE( new_base != ray2.base );
    ray2.move_to( new_base );
    REQUIRE( new_base == ray2.base );
    REQUIRE( ray2.dir == old_dir );
  }
}

TEST_CASE( "Ray Color", "[Ray][Color][core][rendering]" ) {

  Ray<ColorVec, PosVec> ray;
  ColorVec a_color( { 0.5, 0.3, 0.6 } );    // NOLINT
  ColorVec b_color( { 0.1, 0.3, 0.4 } );    // NOLINT
  ColorVec mean_color( { 0.3, 0.3, 0.5 } ); // NOLINT

  ray.sample( a_color );

  SECTION( "A Color" ) { REQUIRE( all_close_3d( a_color, ray.color() ) ); }

  SECTION( "A+B color" ) {
    INFO( "Before: " << ray.color() );
    ray.sample( b_color );
    INFO( "After: " << ray.color() );
    INFO( "Ought: " << mean_color );
    REQUIRE( all_close_3d( mean_color, ray.color() ) );
  }
}
// NOLINTEND(*-cognitive-complexity, *-magic-numbers)
