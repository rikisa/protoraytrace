#include "common/valueconstants.hpp"
#include "core/linalg.hpp"
#include "test_main.hpp"

namespace tv = testvals;
using core::cross;
using core::DArray3;
using core::DArray4;
using core::PosVec;

TEST_CASE( "ArrayN Norm", "[ArrayN][core][linalg][algebra]" ) {

  DArray3 test_3_gt( tv::vec3_gt );
  DArray3 test_3_lt( tv::vec3_lt );

  REQUIRE( is_close( test_3_gt.sum(), tv::vec3_gt_sum ) );
  REQUIRE( is_close( test_3_gt.norm(), tv::vec3_gt_l2 ) );

  REQUIRE( is_close( test_3_lt.sum(), tv::vec3_lt_sum ) );
  REQUIRE( is_close( test_3_lt.norm(), tv::vec3_lt_l2 ) );

  DArray3 base_x( { 1, 0, 0 } );
  DArray3 base_y( { 0, 1, 0 } );
  DArray3 base_z( { 0, 0, 1 } );

  SECTION( "Norm after changes updates itself" ) {
    test_3_gt[ 0 ] = 0;
    test_3_lt[ 0 ] = 0;

    test_3_gt[ 1 ] = 0;
    test_3_lt[ 1 ] = 0;

    test_3_gt.normalize();
    test_3_lt.normalize();

    REQUIRE( is_close( test_3_gt[ 2 ], 1 ) );
    REQUIRE( is_close( test_3_lt[ 2 ], 1 ) );

    REQUIRE( is_close( test_3_gt.norm(), 1 ) );
    REQUIRE( is_close( test_3_lt.norm(), 1 ) );

    REQUIRE( is_close( test_3_gt.sum(), 1 ) );
    REQUIRE( is_close( test_3_lt.sum(), 1 ) );

  } // SECTION

  SECTION( "in-place, L2 normalization" ) {

    test_3_gt.normalize();
    test_3_lt.normalize();

    REQUIRE( is_close( test_3_gt.norm(), 1 ) );
    REQUIRE( is_close( test_3_lt.norm(), 1 ) );

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt_normed[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt_normed[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt_normed[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt_normed[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt_normed[ 2 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt_normed[ 2 ] ) );
  } // SECTION
}

TEST_CASE( "ArrayN Linalg", "[ArrayN][core][linalg][algebra]" ) {

  DArray3 test_3_gt( tv::vec3_gt );
  DArray3 test_3_lt( tv::vec3_lt );

  DArray3 base_y( { 1, 0, 0 } );
  DArray3 base_x( { 0, 1, 0 } );
  DArray3 base_z( { 0, 0, 1 } );

  DArray3 vec_a( { 0.1, 0, 0 } );
  DArray3 vec_b( { 0, 2, 0 } );
  DArray3 cprod( { 0, 2, 0 } );

  SECTION( "member, dot-product" ) {
    REQUIRE( is_close( test_3_gt.dot( test_3_lt ), test_3_lt.dot( test_3_gt ) ) );
    REQUIRE( is_close( test_3_gt.dot( test_3_gt ), test_3_gt.norm() * test_3_gt.norm() ) );
    REQUIRE( is_close( test_3_gt.dot( test_3_lt ), 1.5 ) );
  } // SECTION

  SECTION( "member, cross-product" ) {
    REQUIRE( cross( base_y, base_x ) == base_z );
    REQUIRE( cross( base_x, base_y ) == -1.0 * base_z );

    REQUIRE( cross( base_x, base_y * 2 ) == -2.0 * base_z );

    PosVec i( { 0, -1, -2 } );
    PosVec j( { 0, 1, 0 } );
    PosVec k( { 2, 0, 0 } );

    REQUIRE( cross( i, j ) == k );
  } // SECTION

  SECTION( "member, cross-product, orthogonality" ) {

    PosVec i( { 1, 2, 1 } );
    PosVec j( { 2, 1, -4 } );
    i.normalize();
    j.normalize();
    auto k = cross( i, j );
    INFO( "i: " << i << " j: " << j << " k: " << k );
    REQUIRE( ( cross( j, k ) - i ).sum() < 1e-32 );
    REQUIRE( ( cross( k, i ) - j ).sum() < 1e-32 );
    REQUIRE( ( cross( j, i ) - k * -1 ).sum() < 1e-32 );
    REQUIRE( ( cross( k, j ) - i * -1 ).sum() < 1e-32 );
    REQUIRE( ( cross( i, k ) - j * -1 ).sum() < 1e-32 );
  }
}

TEST_CASE( "ArrayN Trig", "[ArrayN][core][linalg][algebra]" ) {

  DArray3 test_3_gt( tv::vec3_gt );
  DArray3 test_3_lt( tv::vec3_lt );

  DArray3 base_x( { 1, 0, 0 } );
  DArray3 base_y( { 0, 1, 0 } );

  INFO( "Vec3gt cos Vec3lt = " << test_3_gt.cos( test_3_lt ) );
  SECTION( "member, cos" ) {
    REQUIRE( test_3_gt.cos( test_3_gt ) == 1 );
    REQUIRE( base_y.cos( base_x ) == 0 );
    REQUIRE( is_close( test_3_gt.cos( test_3_lt ), tv::vec3_lt_cos_vec3_gt ) );
    REQUIRE( is_close( test_3_lt.cos( test_3_gt ), tv::vec3_lt_cos_vec3_gt ) );
  } // SECTION

} // TEST_CASE
