add_executable( test_Linalg )
target_sources( test_Linalg
    PRIVATE
        test_main.hpp
        test_main.cpp

        algebra.cpp
        arithmetic.cpp
        defaults.cpp
        CoordBase.cpp
)
target_link_libraries( test_Linalg
    PUBLIC
        Catch2::Catch2
        core
        fmt
        test_commons
)

catch_discover_tests( test_Linalg )
