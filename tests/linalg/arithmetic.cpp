#include "common/valueconstants.hpp"
#include "core/linalg.hpp"
#include "test_main.hpp"

namespace tv = testvals;
using core::PosVec;

TEST_CASE( "ArrayN SUB", "[ArrayN][core][linalg][arithmetic]" ) {

  PosVec test_3_gt( tv::vec3_gt );
  PosVec test_3_lt( tv::vec3_lt );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  constexpr double a = 1.3;

  SECTION( "in-place, scalar" ) {
    test_3_gt -= a;
    test_3_lt -= a;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] - a ) );
    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] - a ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] - a ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] - a ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] - a ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] - a ) );
  } // SECTION

  SECTION( "in-place, element-wise" ) {
    test_3_gt -= test_3_lt;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] - tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] - tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] - tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );
  } // SECTION

  SECTION( "infix, element-wise" ) {
    auto result = test_3_gt - test_3_lt;

    REQUIRE( is_close( result[ 0 ], tv::vec3_gt[ 0 ] - tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result[ 1 ], tv::vec3_gt[ 1 ] - tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result[ 2 ], tv::vec3_gt[ 2 ] - tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );
    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] ) );
  } // SECTION

  SECTION( "infix, scalar 1" ) {
    auto result_lt = test_3_lt - a;
    auto result_gt = test_3_gt - a;

    REQUIRE( is_close( result_lt[ 0 ], tv::vec3_lt[ 0 ] - a ) );
    REQUIRE( is_close( result_lt[ 1 ], tv::vec3_lt[ 1 ] - a ) );
    REQUIRE( is_close( result_lt[ 2 ], tv::vec3_lt[ 2 ] - a ) );

    REQUIRE( is_close( result_gt[ 0 ], tv::vec3_gt[ 0 ] - a ) );
    REQUIRE( is_close( result_gt[ 1 ], tv::vec3_gt[ 1 ] - a ) );
    REQUIRE( is_close( result_gt[ 2 ], tv::vec3_gt[ 2 ] - a ) );

  } // SECTION

  SECTION( "infix, scalar 2" ) {
    auto result_lt = a - test_3_lt;
    auto result_gt = a - test_3_gt;

    REQUIRE( is_close( result_lt[ 0 ], a - tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result_lt[ 1 ], a - tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result_lt[ 2 ], a - tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( result_gt[ 0 ], a - tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( result_gt[ 1 ], a - tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( result_gt[ 2 ], a - tv::vec3_gt[ 2 ] ) );

  } // SECTION

} // TEST_CASE

TEST_CASE( "ArrayN ADD", "[ArrayN][core][linalg][arithmetic]" ) {

  PosVec test_3_gt( tv::vec3_gt );
  PosVec test_3_lt( tv::vec3_lt );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  constexpr double a = 3.13541;

  SECTION( "in-place, scalar" ) {
    test_3_gt += a;
    test_3_lt += a;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] + a ) );
    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] + a ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] + a ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] + a ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] + a ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] + a ) );

  } // SECTION

  SECTION( "in-place, element-wise" ) {
    test_3_gt += test_3_lt;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] + tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] + tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] + tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );

  } // SECTION
  SECTION( "infix, element-wise" ) {
    auto result = test_3_gt + test_3_lt;

    REQUIRE( is_close( result[ 0 ], tv::vec3_gt[ 0 ] + tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result[ 1 ], tv::vec3_gt[ 1 ] + tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result[ 2 ], tv::vec3_gt[ 2 ] + tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );
    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] ) );
  } // SECTION

  SECTION( "infix, scalar 1" ) {
    auto result_lt = test_3_lt + a;
    auto result_gt = test_3_gt + a;

    REQUIRE( is_close( result_lt[ 0 ], tv::vec3_lt[ 0 ] + a ) );
    REQUIRE( is_close( result_lt[ 1 ], tv::vec3_lt[ 1 ] + a ) );
    REQUIRE( is_close( result_lt[ 2 ], tv::vec3_lt[ 2 ] + a ) );

    REQUIRE( is_close( result_gt[ 0 ], tv::vec3_gt[ 0 ] + a ) );
    REQUIRE( is_close( result_gt[ 1 ], tv::vec3_gt[ 1 ] + a ) );
    REQUIRE( is_close( result_gt[ 2 ], tv::vec3_gt[ 2 ] + a ) );

  } // SECTION

  SECTION( "infix, scalar 2" ) {
    auto result_lt = a + test_3_lt;
    auto result_gt = a + test_3_gt;

    REQUIRE( is_close( result_lt[ 0 ], a + tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result_lt[ 1 ], a + tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result_lt[ 2 ], a + tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( result_gt[ 0 ], a + tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( result_gt[ 1 ], a + tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( result_gt[ 2 ], a + tv::vec3_gt[ 2 ] ) );

  } // SECTION
} // TEST_CASE

TEST_CASE( "ArrayN MUL", "[ArrayN][core][linalg][arithmetic]" ) {

  PosVec test_3_gt( tv::vec3_gt );
  PosVec test_3_lt( tv::vec3_lt );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  constexpr double a = 1.1112;

  SECTION( "in-place, scalar" ) {
    test_3_gt *= a;
    test_3_lt *= a;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] * a ) );
    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] * a ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] * a ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] * a ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] * a ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] * a ) );

  } // SECTION

  SECTION( "in-place, element-wise" ) {
    test_3_gt *= test_3_lt;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] * tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] * tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] * tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );

  } // SECTION
  SECTION( "infix, element-wise" ) {
    auto result = test_3_gt * test_3_lt;

    REQUIRE( is_close( result[ 0 ], tv::vec3_gt[ 0 ] * tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result[ 1 ], tv::vec3_gt[ 1 ] * tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result[ 2 ], tv::vec3_gt[ 2 ] * tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );
    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] ) );
  } // SECTION

  SECTION( "infix, scalar 1" ) {
    auto result_lt = test_3_lt * a;
    auto result_gt = test_3_gt * a;

    REQUIRE( is_close( result_lt[ 0 ], tv::vec3_lt[ 0 ] * a ) );
    REQUIRE( is_close( result_lt[ 1 ], tv::vec3_lt[ 1 ] * a ) );
    REQUIRE( is_close( result_lt[ 2 ], tv::vec3_lt[ 2 ] * a ) );

    REQUIRE( is_close( result_gt[ 0 ], tv::vec3_gt[ 0 ] * a ) );
    REQUIRE( is_close( result_gt[ 1 ], tv::vec3_gt[ 1 ] * a ) );
    REQUIRE( is_close( result_gt[ 2 ], tv::vec3_gt[ 2 ] * a ) );

  } // SECTION

  SECTION( "infix, scalar 2" ) {
    auto result_lt = a * test_3_lt;
    auto result_gt = a * test_3_gt;

    REQUIRE( is_close( result_lt[ 0 ], a * tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result_lt[ 1 ], a * tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result_lt[ 2 ], a * tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( result_gt[ 0 ], a * tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( result_gt[ 1 ], a * tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( result_gt[ 2 ], a * tv::vec3_gt[ 2 ] ) );

  } // SECTION
} // TEST_CASE

TEST_CASE( "ArrayN DIV", "[ArrayN][core][linalg][arithmetic]" ) {

  PosVec test_3_gt( tv::vec3_gt );
  PosVec test_3_lt( tv::vec3_lt );

  REQUIRE( test_3_gt[ 0 ] != test_3_lt[ 0 ] );
  REQUIRE( test_3_gt[ 1 ] != test_3_lt[ 1 ] );
  REQUIRE( test_3_gt[ 2 ] != test_3_lt[ 2 ] );

  constexpr double a = 1.12358;

  SECTION( "in-place, scalar" ) {
    test_3_gt /= a;
    test_3_lt /= a;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] / a ) );
    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] / a ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] / a ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] / a ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] / a ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] / a ) );

  } // SECTION

  SECTION( "in-place, element-wise" ) {
    test_3_gt /= test_3_lt;

    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] / tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] / tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] / tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );

  } // SECTION
  SECTION( "infix, element-wise" ) {
    auto result = test_3_gt / test_3_lt;

    REQUIRE( is_close( result[ 0 ], tv::vec3_gt[ 0 ] / tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result[ 1 ], tv::vec3_gt[ 1 ] / tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result[ 2 ], tv::vec3_gt[ 2 ] / tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( test_3_lt[ 0 ], tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( test_3_lt[ 1 ], tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( test_3_lt[ 2 ], tv::vec3_lt[ 2 ] ) );
    REQUIRE( is_close( test_3_gt[ 0 ], tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( test_3_gt[ 1 ], tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( test_3_gt[ 2 ], tv::vec3_gt[ 2 ] ) );
  } // SECTION

  SECTION( "infix, scalar 1" ) {
    auto result_lt = test_3_lt / a;
    auto result_gt = test_3_gt / a;

    REQUIRE( is_close( result_lt[ 0 ], tv::vec3_lt[ 0 ] / a ) );
    REQUIRE( is_close( result_lt[ 1 ], tv::vec3_lt[ 1 ] / a ) );
    REQUIRE( is_close( result_lt[ 2 ], tv::vec3_lt[ 2 ] / a ) );

    REQUIRE( is_close( result_gt[ 0 ], tv::vec3_gt[ 0 ] / a ) );
    REQUIRE( is_close( result_gt[ 1 ], tv::vec3_gt[ 1 ] / a ) );
    REQUIRE( is_close( result_gt[ 2 ], tv::vec3_gt[ 2 ] / a ) );

  } // SECTION

  SECTION( "infix, scalar 2" ) {
    auto result_lt = a / test_3_lt;
    auto result_gt = a / test_3_gt;

    REQUIRE( is_close( result_lt[ 0 ], a / tv::vec3_lt[ 0 ] ) );
    REQUIRE( is_close( result_lt[ 1 ], a / tv::vec3_lt[ 1 ] ) );
    REQUIRE( is_close( result_lt[ 2 ], a / tv::vec3_lt[ 2 ] ) );

    REQUIRE( is_close( result_gt[ 0 ], a / tv::vec3_gt[ 0 ] ) );
    REQUIRE( is_close( result_gt[ 1 ], a / tv::vec3_gt[ 1 ] ) );
    REQUIRE( is_close( result_gt[ 2 ], a / tv::vec3_gt[ 2 ] ) );

  } // SECTION
} // TEST_CASE

TEST_CASE( "ArrayN CMP", "[ArrayN][core][linalg][compare]" ) {

  PosVec test_3_lt( tv::vec3_lt );

  SECTION( "bool, element-wise" ) {
    REQUIRE( all_close_3d( test_3_lt, tv::vec3_lt ) );
    REQUIRE( not all_close_3d( test_3_lt, tv::vec3_gt ) );
  } // SECTION

} // TEST_CASE
