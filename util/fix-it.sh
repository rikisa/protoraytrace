#!/bin/bash
SCRIPTDIR=$( dirname -- "$0"; )
ROOTDIR=$( dirname ${SCRIPTDIR} )
SRCDIR=${ROOTDIR}/src/raytracer
BUILDDIR=${ROOTDIR}/build

CHECKS="clang-analyzer-*, modernize-*, hicpp-*, bugprone-*, cppcoreguidelines-*performance-*, portability-*, readability-*"


run-clang-tidy \
    -j10 \
    -p=${BUILDDIR}       \
    -checks="${CHECKS}"  \
    -header-filter=".*"  \
    -fix \
    -format \
    -style=file \
    ${SRCDIR}

# clang-tidy -p=${BUILDDIR}       \
#            -checks="${CHECKS}"  \
#            --dump-config
#            
 
